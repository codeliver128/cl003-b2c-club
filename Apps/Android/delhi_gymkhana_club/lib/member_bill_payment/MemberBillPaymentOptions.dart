import 'package:delhi_gymkhana_club/arguments/WebviewArguments.dart';
import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils/DecimalTextInputFormatter.dart';
import 'package:delhi_gymkhana_club/utils/RadioListWidget.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:delhi_gymkhana_club/constants/Constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';

class MemberBillPaymentOptions extends StatefulWidget {
  @override
  _MemberBillPaymentOptionsState createState() =>
      _MemberBillPaymentOptionsState();
}

class _MemberBillPaymentOptionsState extends State<MemberBillPaymentOptions> {
  String _radioButtonSelected = "";
  bool termsAndConditionAccepted = false;

  List<String> listOfPaymentMode = StringsMyApp.payment_mode_list;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.member_bill_detail,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // _getRows(StringsMyApp.member_id, "P-6564"),
            // _getRows(StringsMyApp.member_name, "Laxman Singh"),
            _getRows(StringsMyApp.bill_date, "03 July 2019"),
            _getRows(StringsMyApp.bill_due_date, "27 July 2019"),
            _getRows(StringsMyApp.bill_amount, "-695.00"),
            _getRows(StringsMyApp.extra_amount, "0"),
            _getRows(StringsMyApp.final_payable, "0"),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                StringsMyApp.mode_of_payment,
                style: TextStyle(
                    color: ColorsMyApp.themeColor, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 4),
            ),
            Expanded(
              child: RadioListWidget(
                callback: (val) => setState(() => _radioButtonSelected = val),
                listOfRadioValues: StringsMyApp.payment_mode_list,
                selectedRadioButton: _radioButtonSelected,
              ),
            ),
            _getTermsAndConditionsRow(),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                    text: StringsMyApp.note_colon,
                    style: TextStyle(
                        color: Colors.grey, fontWeight: FontWeight.bold),
                  ),
                  TextSpan(
                    text: " " + StringsMyApp.select_payment_option_msg,
                    style: TextStyle(color: Colors.grey, fontSize: 12),
                  ),
                ],
                style: TextStyle(
                  color: ColorsMyApp.greyDark,
                ),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: Button(
                    buttonText: StringsMyApp.pay_now,
                    onButtonClickFunction: _clickButton,
                    buttonTextColor: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _clickButton(BuildContext context) {
    //Navigator.pushNamed(context, FORGOT_PASSWORD);
    print("Laxman singh" + _radioButtonSelected);
    if (_radioButtonSelected.isEmpty) {
      Toast.show(StringsMyApp.pls_select_payment_option, context,
          gravity: Toast.BOTTOM);
      return;
    }

    if (!termsAndConditionAccepted) {
      Toast.show(StringsMyApp.accept_terms_conditions, context,
          gravity: Toast.BOTTOM);
      return;
    }

    Toast.show("Bill Payment Successful.", context, gravity: Toast.BOTTOM);
    Navigator.popUntil(context, ModalRoute.withName(HOME_SCREEN));
  }

  _getRows(String title, String value) {
    return Container(
        margin: EdgeInsets.only(top: 4),
        color: ColorsMyApp.grey_21,
        padding: EdgeInsets.only(top: 15, bottom: 15, left: 12, right: 12),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text(
                title,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: ColorsMyApp.greyDark,
                    fontSize: 13),
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(
                value,
                textAlign: TextAlign.right,
                style: TextStyle(color: ColorsMyApp.greyDark, fontSize: 13),
              ),
            )
          ],
        ));
  }

  _getTermsAndConditionsRow() {
    return Container(
      child: Row(
        children: <Widget>[
          Checkbox(
            value: termsAndConditionAccepted,
            activeColor: ColorsMyApp.VividOrange,
            onChanged: (bool value) {
              setState(() {
                termsAndConditionAccepted = value;
              });
            },
          ),
          Expanded(
            child: RichText(
              text: TextSpan(
                text: StringsMyApp.agree_with,
                children: <TextSpan>[
                  TextSpan(
                      text: StringsMyApp.terms_and_conditions,
                      style: TextStyle(
                        color: Colors.blueAccent,
                        decoration: TextDecoration.underline,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Navigator.pushNamed(context, WEBVIEW_SCREEN,
                              arguments: WebviewArguments(
                                  url:
                                      "https://delhigymkhana.org.in/termandcandition.aspx",
                                  title: StringsMyApp.terms_and_conditions));
                        }),
                ],
                style: TextStyle(
                  color: ColorsMyApp.greyDark,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
