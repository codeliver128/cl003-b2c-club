import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils/DecimalTextInputFormatter.dart';
import 'package:delhi_gymkhana_club/utils_widget/BulletWidget.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:delhi_gymkhana_club/constants/Constants.dart';

class MemberBillPaymentDetail extends StatefulWidget {
  @override
  _MemberBillPaymentDetailState createState() =>
      _MemberBillPaymentDetailState();
}

class _MemberBillPaymentDetailState extends State<MemberBillPaymentDetail> {
  bool isPreviousSelected;

  @override
  Widget build(BuildContext context) {
    isPreviousSelected = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.member_bill_detail,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 10, left: 16, right: 16, bottom: 16),
        child: Column(
          children: <Widget>[
            _getRows(StringsMyApp.opening_balance_A, isPreviousSelected ?"708.00": "13706.00"),
            _getRows(StringsMyApp.payment_received_B, isPreviousSelected ?"8.00":"15000.00"),
            _getRows(StringsMyApp.arrears_c,isPreviousSelected ?"700.00": "1294.00"),
            _getRows(StringsMyApp.current_bill_amt_D,isPreviousSelected ?"13005.96": "599.44"),
            _getRows(StringsMyApp.amt_payable_E, isPreviousSelected ?"13706.00":"695.00"),
            isPreviousSelected ? Container() :  Container(
              margin: EdgeInsets.only(top: 4),
              padding: EdgeInsets.only(top: 15, bottom: 15, right: 12),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Text(
                      StringsMyApp.pay_extra_amt,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 12),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      alignment: Alignment.topRight,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: ColorsMyApp.grey_border,
                          width: 1,
                        ),
                        // borderRadius: BorderRadius.circular(20.0),
                      ),
                      child: Row(
                        children: <Widget>[
                          new Padding(
                            padding: EdgeInsets.only(left: 10),
                          ),
                          new Expanded(
                            child: TextField(
                              inputFormatters: [
                                DecimalTextInputFormatter(decimalRange: 2)
                              ],
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true),
                              cursorColor: ColorsMyApp.VividOrange,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                labelStyle: TextStyle(color: Colors.black),
                                border: InputBorder.none,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            isPreviousSelected ? Container() : NoteSection(),
            isPreviousSelected
                ? Container()
                : Expanded(
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: Button(
                          buttonText: StringsMyApp.proceed_payment,
                          onButtonClickFunction: _clickButton,
                          buttonTextColor: Colors.white,
                        ),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }

  _clickButton(BuildContext context) {
    Navigator.pushNamed(context, MEMBER_BILL_PAYMENT_OPTIONS);
  }

  int _radioValue1 = 1;

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;
    });
  }

  _getRows(String title, String value) {
    return Container(
        margin: EdgeInsets.only(top: 4),
        color: ColorsMyApp.grey_21,
        padding: EdgeInsets.only(top: 15, bottom: 15, left: 12, right: 12),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text(
                title,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: ColorsMyApp.greyDark,
                    fontSize: 13),
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(
                value,
                textAlign: TextAlign.right,
                style: TextStyle(color: ColorsMyApp.greyDark, fontSize: 13),
              ),
            )
          ],
        ));
  }
}

class NoteSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            StringsMyApp.note_colon,
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          Divider(),
          BulletWidget(
            text: StringsMyApp.payment_note_1,
            bulletColor: ColorsMyApp.greyDark,
          ),
          BulletWidget(
            text: StringsMyApp.payment_note_2,
            bulletColor: ColorsMyApp.greyDark,
          ),
        ],
      ),
    );
  }
}
