import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/InputBoxRectangular.dart';
import 'package:flutter/material.dart';
import 'package:delhi_gymkhana_club/constants/Constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';

class MemberBillPayment extends StatefulWidget {
  @override
  _MemberBillPaymentState createState() => _MemberBillPaymentState();
}

class _MemberBillPaymentState extends State<MemberBillPayment> {
  bool isPreviousSelected = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.member_bill_payment,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        actions: <Widget>[HomeIconToolbar()],
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 16),
        child: Column(
          children: <Widget>[
            Theme(
              data: ThemeData(
                  unselectedWidgetColor: Colors.white,
                  highlightColor: Colors.black),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(
                      color: ColorsMyApp.themeColor,
                      child: Row(
                        children: <Widget>[
                          new Radio(
                            value: 0,
                            activeColor: Colors.black,
                            groupValue: _radioValue1,
                            onChanged: _handleRadioValueChange1,
                          ),
                          new Text(
                            StringsMyApp.previous_bill,
                            style: new TextStyle(
                                color: Colors.white, fontSize: 16.0),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: 20,
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      color: ColorsMyApp.themeColor,
                      child: Row(
                        children: <Widget>[
                          new Radio(
                            value: 1,
                            activeColor: Colors.black,
                            groupValue: _radioValue1,
                            onChanged: _handleRadioValueChange1,
                          ),
                          new Text(
                            StringsMyApp.current_bill,
                            style: new TextStyle(
                                color: Colors.white, fontSize: 16.0),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 16, bottom: 16, right: 4),
              child: Container(
                alignment: Alignment.centerRight,
                child: Text(
                  StringsMyApp.view_bill,
                  style: TextStyle(fontSize: 15, color: ColorsMyApp.themeColor),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10, top: 1),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Text(
                      isPreviousSelected?StringsMyApp.previous_bill:
                      StringsMyApp.current_bill,
                      style: TextStyle(color: Colors.black, fontSize: 18),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text(
                      isPreviousSelected?"Total payment: 13706.00":StringsMyApp.total_payment_due_colon + "695.00",
                      textAlign: TextAlign.right,
                      style:
                          TextStyle(color: ColorsMyApp.greyDark, fontSize: 14),
                    ),
                  )
                ],
              ),
            ),


            _getRows(StringsMyApp.bill_no, isPreviousSelected?"5411":"5511"),
            _getRows(StringsMyApp.bill_date, isPreviousSelected?"22 April 2019": "22 May 2019"),
            _getRows(StringsMyApp.bill_month,  isPreviousSelected?"March":"April"),
            _getRows(StringsMyApp.staus,  isPreviousSelected?"Active":"Active"),
            _getRows(StringsMyApp.due_date,  isPreviousSelected?"11 May 2019":"11 June 2019"),



            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: Button(
                    buttonText: StringsMyApp.next,
                    onButtonClickFunction: _clickButton,
                    buttonTextColor: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _clickButton(BuildContext context) {
    Navigator.pushNamed(context, MEMBER_BILL_PAYMENT_DETAIL,arguments: isPreviousSelected);
  }

  int _radioValue1 = 1;

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;
      if (value == 0) {
        isPreviousSelected = true;
      } else {
        isPreviousSelected = false;
      }
    });
  }

  _getRows(String title, String value) {
    return Container(
      margin: EdgeInsets.only(top: 4),
      color: ColorsMyApp.grey_21,
      padding: EdgeInsets.only(top: 15, bottom: 15, left: 12, right: 12),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text(
              title,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: ColorsMyApp.greyDark,
                  fontSize: 13),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              value,
              textAlign: TextAlign.right,
              style: TextStyle(color: ColorsMyApp.greyDark, fontSize: 13),
            ),
          )
        ],
      ),
    );
  }
}
