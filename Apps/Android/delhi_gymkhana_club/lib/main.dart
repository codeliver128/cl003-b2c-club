import 'dart:async';

import 'package:delhi_gymkhana_club/change_password/ChangePasswordScreen.dart';
import 'package:delhi_gymkhana_club/members_directory/MembersDirectoryScreen.dart';
import 'package:delhi_gymkhana_club/online_reservations/room_reservations/RoomReservationsScreen.dart';
import 'package:delhi_gymkhana_club/profile/EditProfileScreen.dart';
import 'package:delhi_gymkhana_club/profile/ViewProfileScreen.dart';
import 'package:delhi_gymkhana_club/transaction_history/PartyVenueBookingDetailScreen.dart';
import 'package:delhi_gymkhana_club/transaction_history/PaymentDetailScreen.dart';
import 'package:delhi_gymkhana_club/transaction_history/PosTransactionDetailScreen.dart';
import 'package:delhi_gymkhana_club/transaction_history/RoomBookingDetailScreen.dart';
import 'package:delhi_gymkhana_club/transaction_history/TransactionHistoryScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import './splash/SplashScreen.dart';
import 'package:delhi_gymkhana_club/webview/WebViewScreen.dart';
import 'constants/Constants.dart';
import 'events_calendar/EventsCalendar.dart';
import 'feedback/FeedBackScreen.dart';
import 'forgotpassword/ForgotPasswordScreen.dart';
import 'home/HomeScreen.dart';
import 'package:delhi_gymkhana_club/login/LoginScreen.dart';
import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';

import 'home_delivery_order/HomeDeliveryBillAndPayScreen.dart';
import 'home_delivery_order/HomeDeliveryOrderScreen.dart';
import 'home_delivery_order/SelectHomeDeliveryOrderScreen.dart';
import 'important_notices/ImportantNoticeTypesList.dart';
import 'important_notices/ImportantNoticesScreen.dart';
import 'member_bill_payment/MemberBillPayment.dart';
import 'member_bill_payment/MemberBillPaymentDetail.dart';
import 'member_bill_payment/MemberBillPaymentOptions.dart';
import 'membershipenquiry/MemberShipEnquiryScreen.dart';
import 'online_reservations/OnlineReservationsScreen.dart';
import 'online_reservations/books_reservation/BookDetailScreen.dart';
import 'online_reservations/books_reservation/BooksReservationScreen.dart';
import 'online_reservations/party_venue_reservation/AddVenueScreen.dart';
import 'online_reservations/party_venue_reservation/PartyVenueAvailabllityScreen.dart';
import 'online_reservations/party_venue_reservation/PartyVenueBillDetailScreen.dart';
import 'online_reservations/room_reservations/RoomBookingScreen.dart';
import 'online_reservations/room_reservations/RoomReservationBillingScreen.dart';
import 'online_reservations/sports_slot_reservation/SportSlotReservationScreen.dart';

Future main() async {
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    // systemNavigationBarColor: Colors.blue, // navigation bar color
    statusBarColor: ColorsMyApp.VividOrange, // status bar color
  ));

  runApp(new MaterialApp(
    title: 'GymKhana Club',
    debugShowCheckedModeBanner: false,
    home: SplashScreen(),
    routes: <String, WidgetBuilder>{
      LOGIN_SCREEN: (BuildContext context) => LoginScreen(),
      HOME_SCREEN: (BuildContext context) => HomeScreen(),
      FORGOT_PASSWORD: (BuildContext context) => ForGotPasswordScreen(),
      MEMBERSHIP_ENQUIRY: (BuildContext context) => MemberShipEnquiryScreen(),
      MEMBER_BILL_PAYMENT: (BuildContext context) => MemberBillPayment(),
      MEMBER_BILL_PAYMENT_DETAIL: (BuildContext context) => MemberBillPaymentDetail(),
      MEMBER_BILL_PAYMENT_OPTIONS: (BuildContext context) => MemberBillPaymentOptions(),
      WEBVIEW_SCREEN: (BuildContext context) => WebViewScreen(),
      VIEW_PROFILE_SCREEN: (BuildContext context) => ViewProfileScreen(),
      EDIT_PROFILE_SCREEN:(BuildContext context) => EditProfileScreen(),
      EVENTS_CALENDAR:(BuildContext context) => EventsCalendar(),
      IMPORTANT_NOTICES_SCREEN:(BuildContext context) => ImportantNoticesScreen(),
      IMPORTANT_NOTICE_TYPES_LIST:(BuildContext context) =>  ImportantNoticeTypesList(),
      TRANSACTION_HISTORY_SCREEN:(BuildContext context) => TransactionHistoryScreen(),
      POS_TRANSACTION_DETAIL_SCREEN:(BuildContext context) => PosTransactionDetailScreen(),
      ROOM_BOOKING_DETAIL_SCREEN:(BuildContext context) => RoomBookingDetailScreen(),
      PARTY_VENUE_BOOKING_DETAIL_SCREEN:(BuildContext context) =>  PartyVenueBookingDetailScreen(),
      PAYMENT_DETAIL_SCREEN:(BuildContext context) => PaymentDetailScreen(),
      ONLINE_RESERVATIONS_SCREEN:(BuildContext context) => OnlineReservationsScreen(),
      SPORT_SLOT_RESERVATION_SCREEN:(BuildContext context) => SportSlotReservationScreen(),
      BOOKS_RESERVATION_SCREEN:(BuildContext context) => BooksReservationScreen(),
      BOOK_DETAIL_SCREEN:(BuildContext context) => BookDetailScreen(),
      ROOM_RESERVATIONS_SCREEN:(BuildContext context) => RoomReservationsScreen(),
      ROOM_BOOKING_SCREEN:(BuildContext context) => RoomBookingScreen(),
      ROOM_RESERVATION_BILLING_SCREEN:(BuildContext context) => RoomReservationBillingScreen(),
      PARTY_VENUE_AVAILABLLITY_SCREEN:(BuildContext context) => PartyVenueAvailabllityScreen(),
      MEMBERS_DIRECTORY_SCREEN:(BuildContext context) => MembersDirectoryScreen(),
      FEEDBACK_SCREEN:(BuildContext context) => FeedBackScreen(),
      CHANGE_PASSWORD_SCREEN:(BuildContext context) => ChangePasswordScreen(),
      HOME_DELIVERY_ORDER_SCREEN:(BuildContext context) => HomeDeliveryOrderScreen(),
      SELECT_HOMEDELIVERY_ORDER_SCREEN:(BuildContext context) => SelectHomeDeliveryOrderScreen(),
      ADD_VENUE_SCREEN:(BuildContext context) => AddVenueScreen(),
      PARTY_VENUE_BILL_DETAIL_SCREEN:(BuildContext context) => PartyVenueBillDetailScreen(),
      HOME_DELIVERY_BILLANDPAY_SCREEN:(BuildContext context) => HomeDeliveryBillAndPayScreen(),
    },
  ));
}
