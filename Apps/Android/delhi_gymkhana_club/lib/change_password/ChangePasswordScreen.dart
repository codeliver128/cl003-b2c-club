import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/ImageAssetsConstants.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';

class ChangePasswordScreen extends StatefulWidget {
  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  String _oldPassword, _newPassword, _confirmPassword;
  bool _isValidForm = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.change_password,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.only(top: 16, left: 16, right: 16),
        child: ListView(
          children: <Widget>[
            Image.asset(ImageAssetsConstants.app_logo),
            Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.editBoxBorderColor,
                  width: 1,
                ),
                // borderRadius: BorderRadius.circular(20.0),
              ),
              margin: const EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                children: <Widget>[
                  new Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 6.0),
                  ),
                  new Expanded(
                    child: TextField(
                      onChanged: (text) {
                        _oldPassword = text;
                      },
                      obscureText: true,
                      cursorColor: ColorsMyApp.VividOrange,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(color: Colors.white),
                        border: InputBorder.none,
                        hintText: StringsMyApp.old_password,
                        hintStyle: TextStyle(color: ColorsMyApp.greyDark),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.editBoxBorderColor,
                  width: 1,
                ),
                // borderRadius: BorderRadius.circular(20.0),
              ),
              margin: const EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                children: <Widget>[
                  new Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 6.0),
                  ),
                  new Expanded(
                    child: TextField(
                      onChanged: (text) {
                        _newPassword = text;
                      },
                      obscureText: true,
                      cursorColor: ColorsMyApp.VividOrange,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(color: Colors.white),
                        border: InputBorder.none,
                        hintText: StringsMyApp.new_password,
                        hintStyle: TextStyle(color: ColorsMyApp.greyDark),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.editBoxBorderColor,
                  width: 1,
                ),
                // borderRadius: BorderRadius.circular(20.0),
              ),
              margin: const EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                children: <Widget>[
                  new Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 6.0),
                  ),
                  new Expanded(
                    child: TextField(
                      onChanged: (text) {
                        _confirmPassword = text;
                        if (_oldPassword == _newPassword) {
                          setState(() {
                            _isValidForm = false;
                          });

                          Toast.show(
                              "Old Password and New Password can not be same.",
                              context,
                              gravity: Toast.CENTER,
                              duration: Toast.LENGTH_LONG);
                        } else {
                          setState(() {
                            _isValidForm = true;
                          });
                        }
                      },
                      obscureText: true,
                      cursorColor: ColorsMyApp.VividOrange,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(color: Colors.white),
                        border: InputBorder.none,
                        hintText: StringsMyApp.confirm_password,
                        hintStyle: TextStyle(color: ColorsMyApp.greyDark),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Button(
                buttonText: StringsMyApp.submit,
                onButtonClickFunction: _callChangePasswordApi,
              ),
            )
          ],
        ),
      ),
    );
  }

  void _callChangePasswordApi(BuildContext context) {
    if (_newPassword != _confirmPassword) {
      Toast.show("New Password and Confirm Password not matched.", context,
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      return;
    }

    Toast.show(_isValidForm.toString(), context,
        gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
  }
}
