import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/utils_widget/InputBoxRectangular.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class MemberShipEnquiryScreen extends StatefulWidget {
  @override
  _MemberShipEnquiryScreenState createState() =>
      _MemberShipEnquiryScreenState();
}

class _MemberShipEnquiryScreenState extends State<MemberShipEnquiryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.membership_enquiry,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.only(top: 16, left: 16, right: 16),
        child: Column(
          children: <Widget>[
            InputBoxRectangular(
              hintText: StringsMyApp.name,
              isPasswordField: false,
              hintColor: ColorsMyApp.greyDark,
              boxBorderColor: ColorsMyApp.editBoxBorderColor,
            ),
            InputBoxRectangular(
              hintText: StringsMyApp.mobile_no,
              isPasswordField: false,
              hintColor: ColorsMyApp.greyDark,
              boxBorderColor: ColorsMyApp.editBoxBorderColor,
            ),
            InputBoxRectangular(
              hintText: StringsMyApp.email_id,
              isPasswordField: false,
              hintColor: ColorsMyApp.greyDark,
              boxBorderColor: ColorsMyApp.editBoxBorderColor,
            ),
            InputBoxRectangular(
              hintText: StringsMyApp.message,
              isPasswordField: false,
              hintColor: ColorsMyApp.greyDark,
              boxBorderColor: ColorsMyApp.editBoxBorderColor,
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Button(
                buttonText: StringsMyApp.submit,
                onButtonClickFunction: _callMembershipEnquiryApi,
              ),
            )
          ],
        ),
      ),
    );
  }

  void _callMembershipEnquiryApi() {}
}
