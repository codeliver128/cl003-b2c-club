import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';

class Button extends StatelessWidget {
  final String buttonText;
  final Function onButtonClickFunction;
  final Color buttonTextColor;
  final Color buttonBackGroundColor;

  Button({this.buttonText, this.onButtonClickFunction, buttonTextColor,buttonBackGroundColor})
      : buttonTextColor =
            buttonTextColor == null ? ColorsMyApp.buttonTextColor : buttonTextColor,
        buttonBackGroundColor =
        buttonBackGroundColor == null ? ColorsMyApp.themeColor : buttonBackGroundColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        child: Text(buttonText,style: TextStyle(fontSize: 16),),
        padding: EdgeInsets.only(left: 0, right: 0, top: 16.0, bottom: 16.0),
        color: buttonBackGroundColor,
        elevation: 4,
        textColor: buttonTextColor,
        onPressed: () {
          onButtonClickFunction(context);
        },
      ),
    );
  }
}
