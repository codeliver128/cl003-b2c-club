import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:delhi_gymkhana_club/constants/Constants.dart';

class HomeIconToolbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        Icons.home,
        color: Colors.black,
      ),
      onPressed: () {
        Navigator.popUntil(context, ModalRoute.withName(HOME_SCREEN));
      },
    );
  }
}
