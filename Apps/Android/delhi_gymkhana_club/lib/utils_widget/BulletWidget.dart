import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BulletWidget extends StatelessWidget {
  final String text;
  final double bulletCircleHeight;
  final double bulletCircleWidth;
  final Color bulletColor;
  final Color textColor;

  BulletWidget(
      {this.text,
      bulletCircleHeight,
      bulletCircleWidth,
      bulletColor,
      textColor})
      : this.bulletCircleHeight =
            bulletCircleHeight == null ? 8.0 : bulletCircleHeight,
        this.bulletCircleWidth =
            bulletCircleWidth == null ? 8.0 : bulletCircleWidth,
        this.bulletColor = bulletColor == null ? Colors.black : bulletColor,
        this.textColor = textColor == null ? Colors.black : textColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // Bullet Circle
          Container(
            margin: EdgeInsets.only(top: 3),
            height: bulletCircleHeight,
            width: 8.0,
            decoration: new BoxDecoration(
              color: bulletColor,
              shape: BoxShape.circle,
            ),
          ),
          // End of Bullet Circle

          // Text Content
          Padding(
            padding: EdgeInsets.only(right: 8),
          ),
          Flexible(
            child: new Text(
              text,
              style: TextStyle(fontSize: 12, color: textColor),
            ),
          )
          // End of Text Content
        ],
      ),
    );
  }
}
