import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class NoDataWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.hourglass_empty,
              size: 40,
            ),
            Text(
              "No Data.",
              style: TextStyle(color: ColorsMyApp.grey_54, fontSize: 18),
            )
          ],
        ),
      ),
    );
  }
}
