import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';

class InputBoxRectangular extends StatelessWidget {
  final String hintText;
  final bool isPasswordField;
  Color _boxBorderColor;
  Color _hintColor;
  Color _textColor;

  InputBoxRectangular(
      {this.hintText,
      this.isPasswordField,
      boxBorderColor,
      hintColor,
      textColor})
      : _boxBorderColor =
            boxBorderColor == null ? ColorsMyApp.VividOrange : boxBorderColor,
        _hintColor = hintColor == null ? ColorsMyApp.VividOrange : hintColor,
        _textColor = textColor == null ? Colors.white : textColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: _boxBorderColor,
          width: 1,
        ),
        // borderRadius: BorderRadius.circular(20.0),
      ),
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 6.0),
          ),
          new Expanded(
            child: TextField(
              obscureText: isPasswordInputField(isPasswordField),
              cursorColor: ColorsMyApp.VividOrange,
              style: TextStyle(color:_textColor),
              decoration: InputDecoration(
                labelStyle: TextStyle(color: Colors.white),
                border: InputBorder.none,
                hintText: hintText,
                hintStyle: TextStyle(color: _hintColor),
              ),
            ),
          )
        ],
      ),
    );
  }

  bool isPasswordInputField(bool isPassword) {
    return isPassword ? true : false;
  }
}
