import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/utils_widget/InputBoxRectangular.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:delhi_gymkhana_club/constants/ImageAssetsConstants.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';

class ForGotPasswordScreen extends StatefulWidget {
  @override
  _ForGotPasswordScreenState createState() => _ForGotPasswordScreenState();
}

class _ForGotPasswordScreenState extends State<ForGotPasswordScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.forgot_password,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 16, left: 16, right: 16),
        child: Column(
          children: <Widget>[
            Image.asset(ImageAssetsConstants.app_logo),
            InputBoxRectangular(
              hintText: StringsMyApp.memberID,
              isPasswordField: false,
              hintColor: ColorsMyApp.greyDark,
              boxBorderColor: ColorsMyApp.editBoxBorderColor,
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Button(
                buttonText: StringsMyApp.submit,
                onButtonClickFunction: _callForgotPasswordApi,
              ),
            ) ],
        ),
      ),
    );
  }

  void _callForgotPasswordApi(){

  }
}
