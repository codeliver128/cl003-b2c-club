import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/Constants.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/members_directory/models/MemberDirectoryModel.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';

import 'models/FeedBackTypeModel.dart';

class FeedBackScreen extends StatefulWidget {
  @override
  _FeedBackScreenState createState() => _FeedBackScreenState();
}

class _FeedBackScreenState extends State<FeedBackScreen> {
  String subject, comments;

  FeedBackTypeModel _selectedFeedBackType;

  List<FeedBackTypeModel> listFeedbackType =
      FeedBackTypeModel().getFeedbackList();
  List<DropdownMenuItem<FeedBackTypeModel>> _dropdownMenuItems;

  @override
  void initState() {
    _dropdownMenuItems = buildDropdownMenuItems(listFeedbackType);
    super.initState();
  }

  List<DropdownMenuItem<FeedBackTypeModel>> buildDropdownMenuItems(
      List feedback) {
    List<DropdownMenuItem<FeedBackTypeModel>> items = List();
    for (FeedBackTypeModel _feedback in feedback) {
      items.add(
        DropdownMenuItem(
          value: _feedback,
          child: Text(_feedback.feedbackTypeTitle),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(FeedBackTypeModel selectedFeedBack) {
    setState(() {
      _selectedFeedBackType = selectedFeedBack;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.feedback,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 8, right: 10, left: 10, bottom: 10),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              StringsMyApp.feedback_health_club,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: Colors.black),
            ),
            Container(
              margin: EdgeInsets.only(left: 0, right: 0, top: 12, bottom: 12),
              padding: EdgeInsets.only(left: 10, right: 10),
              width: double.infinity,
              height: 48,
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.grey_border,
                  width: 1.5,
                ),
              ),
              child: DropdownButton(
                  underline: Container(),
                  isExpanded: true,
                  icon: Icon(
                    Icons.keyboard_arrow_down,
                    color: ColorsMyApp.greyDark,
                    size: 32,
                  ),
                  hint: Text(
                    StringsMyApp.select_one,
                    style: TextStyle(color: ColorsMyApp.grey_54, fontSize: 16),
                  ),
                  value: _selectedFeedBackType,
                  items: _dropdownMenuItems,
                  onChanged: onChangeDropdownItem),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                StringsMyApp.subject,
                style: TextStyle(
                    color: ColorsMyApp.themeColor, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              height: 50,
              padding: EdgeInsets.only(left: 10, right: 10),
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.editBoxBorderColor,
                  width: 1,
                ),
                // borderRadius: BorderRadius.circular(20.0),
              ),
              margin: const EdgeInsets.symmetric(vertical: 10.0),
              child: TextField(
                onChanged: (text) {
                  setState(() {
                    subject = text;
                  });
                },
                cursorColor: ColorsMyApp.VividOrange,
                keyboardType: TextInputType.text,
                maxLines: 5,
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    labelStyle: TextStyle(color: Colors.black),
                    border: InputBorder.none,
                    hintText: StringsMyApp.enter_subject_hint),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                StringsMyApp.comment,
                style: TextStyle(
                    color: ColorsMyApp.themeColor, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              height: 160,
              padding: EdgeInsets.only(left: 10, right: 10),
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.editBoxBorderColor,
                  width: 1,
                ),
                // borderRadius: BorderRadius.circular(20.0),
              ),
              margin: const EdgeInsets.symmetric(vertical: 10.0),
              child: TextField(
                onChanged: (text) {
                  setState(() {
                    comments = text;
                  });
                },
                cursorColor: ColorsMyApp.VividOrange,
                keyboardType: TextInputType.text,
                maxLines: 5,
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    labelStyle: TextStyle(color: Colors.black),
                    border: InputBorder.none,
                    hintText: StringsMyApp.enter_comment_hint),
              ),
            ),
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Button(
                  buttonText: StringsMyApp.submit,
                  buttonBackGroundColor: ColorsMyApp.themeColor,
                  onButtonClickFunction: _clickButton,
                  buttonTextColor: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _clickButton(BuildContext context) {
    if (_selectedFeedBackType == null) {
      Toast.show(StringsMyApp.select_feedback, context, gravity: Toast.BOTTOM);
      return;
    }
    if (subject == null || subject == "") {
      Toast.show(StringsMyApp.enter_subject, context, gravity: Toast.BOTTOM);
      return;
    }
    if (comments == null || comments == "") {
      Toast.show(StringsMyApp.enter_comment, context, gravity: Toast.BOTTOM);
      return;
    }
    Toast.show("FeedBack submitted successfully.", context, gravity: Toast.BOTTOM);

    Navigator.popUntil(context, ModalRoute.withName(HOME_SCREEN));
  }
}
