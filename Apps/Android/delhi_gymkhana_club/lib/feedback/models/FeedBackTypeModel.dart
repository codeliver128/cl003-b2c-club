class FeedBackTypeModel {
  String _id;
  String _feedbackTypeTitle;

  FeedBackTypeModel({id, feedbackTypeTitle})
      : _id = id,
        _feedbackTypeTitle = feedbackTypeTitle;


  String get id => _id;

  set id(String value) {
    _id = value;
  }

  getFeedbackList() {
    return [
      FeedBackTypeModel(id: "1", feedbackTypeTitle: "Praise"),
      FeedBackTypeModel(id: "2", feedbackTypeTitle: "Complaint"),
      FeedBackTypeModel(id: "3", feedbackTypeTitle: "Suggestions")
    ];
  }

  String get feedbackTypeTitle => _feedbackTypeTitle;

  set feedbackTypeTitle(String value) {
    _feedbackTypeTitle = value;
  }
}
