import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/ImageAssetsConstants.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils/NavigationRouteUtil.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';

import 'models/FoodServicesModel.dart';

class HomeDeliveryOrderScreen extends StatefulWidget {
  @override
  _HomeDeliveryOrderScreenState createState() =>
      _HomeDeliveryOrderScreenState();
}

class _HomeDeliveryOrderScreenState extends State<HomeDeliveryOrderScreen> {
  FoodServicesModel _selectedFoodServiceType;
  List<FoodServicesModel> listFoodServiceType =
      FoodServicesModel().getFoodServicesTypes();
  List<DropdownMenuItem<FoodServicesModel>> _dropdownMenuItems;

  @override
  void initState() {
    _dropdownMenuItems = buildDropdownMenuItems(listFoodServiceType);
    super.initState();
  }

  List<DropdownMenuItem<FoodServicesModel>> buildDropdownMenuItems(
      List FoodServiceType) {
    List<DropdownMenuItem<FoodServicesModel>> items = List();
    for (FoodServicesModel foodservicetype in FoodServiceType) {
      items.add(
        DropdownMenuItem(
          value: foodservicetype,
          child: Text(foodservicetype.foodServiceTypeName),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(FoodServicesModel selectedFoodServiceType) {
    setState(() {
      _selectedFoodServiceType = selectedFoodServiceType;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.home_delivery_order,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.only(top: 16, left: 16, right: 16),
        child: ListView(
          children: <Widget>[
            Image.asset(ImageAssetsConstants.app_logo),
            Padding(
              padding: EdgeInsets.only(top: 16, bottom: 16),
              child: Text(
                "8-A, POWAR, DHARAMBIR ENCLAVE, PANAMPILLY NAGAR, Ernakulam - 682036, KERALA, India",
                style: TextStyle(color: ColorsMyApp.grey_54, fontSize: 16),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              width: double.infinity,
              height: 48,
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.grey_border,
                  width: 1.5,
                ),
              ),
              child: DropdownButton(
                  underline: Container(),
                  isExpanded: true,
                  icon: Icon(
                    Icons.keyboard_arrow_down,
                    color: ColorsMyApp.greyDark,
                    size: 32,
                  ),
                  hint: Text(
                    StringsMyApp.select_one,
                    style: TextStyle(color: ColorsMyApp.grey_54, fontSize: 16),
                  ),
                  value: _selectedFoodServiceType,
                  items: _dropdownMenuItems,
                  onChanged: onChangeDropdownItem),
            ),
            Padding(
              padding: EdgeInsets.only(top: 40),
              child: Button(
                buttonText: StringsMyApp.search,
                onButtonClickFunction: _callChangePasswordApi,
                buttonTextColor: Colors.white,
              ),
            )
          ],
        ),
      ),
    );
  }

  void _callChangePasswordApi(BuildContext context) {
    NavigationRouteUtil().navigateToScreen(
        context, StringsMyApp.select_home_delivery_order_screen);
  }
}
