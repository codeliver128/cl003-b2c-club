import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/Constants.dart';
import 'package:delhi_gymkhana_club/constants/IconsConstants.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';

import 'SelectHomeDeliveryOrderScreen.dart';

class HomeDeliveryBillAndPayScreen extends StatefulWidget {
  @override
  _HomeDeliveryBillAndPayScreenState createState() =>
      _HomeDeliveryBillAndPayScreenState();
}

class _HomeDeliveryBillAndPayScreenState
    extends State<HomeDeliveryBillAndPayScreen> {
  List<FoodItems> data = [
    FoodItems(title: "Veg Manchurain", price: "120", isVeg: true, quantity: 2),
    FoodItems(title: "Chicken Tikka", price: "120", isVeg: false, quantity: 3),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.home_delivery_order,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 2, bottom: 10),
        child: Column(
          children: <Widget>[
            Expanded(
                child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Card(
                    child: ListView.builder(
                      itemBuilder: (BuildContext context, int index) =>
                          _buildChilds(data[index], index),
                      itemCount: data.length,
                      primary: false,
                      shrinkWrap: true,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 16, right: 16),
                    child: Text(
                      StringsMyApp.bill_details,
                      style: TextStyle(
                          color: ColorsMyApp.black_low,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Divider(),
                  _getPaymentDetailRow("Items Total", "240.00"),
                  _getPaymentDetailRow("Restaurant Charges", "40.00"),
                  _getPaymentDetailRow("Delivery Fee", "24.00"),
                  Divider(),
                  Container(
                      padding: EdgeInsets.only(top: 10, left: 16, right: 16),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Text(
                                "To Pay",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14,
                                ),
                              )),
                          Expanded(
                            flex: 1,
                            child: Text(
                              "304.00",
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ],
                      ))
                ],
              ),
            )),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(top: 10, left: 16, right: 16),
                child: Button(
                  buttonText: StringsMyApp.pay_now,
                  onButtonClickFunction: (BuildContext context) {
                    Toast.show("Food Ordered Successfully.", context,
                        gravity: Toast.BOTTOM, duration: Toast.LENGTH_SHORT);
                    Navigator.popUntil(context, ModalRoute.withName(HOME_SCREEN));
                  },
                  buttonBackGroundColor: ColorsMyApp.themeColor,
                  buttonTextColor: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _getPaymentDetailRow(String firstColumnTitle, String secondColumnValue) {
    return Container(
        padding: EdgeInsets.only(top: 8, left: 16, right: 16),
        child: Row(
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Text(
                  firstColumnTitle,
                  style: TextStyle(
                    color: ColorsMyApp.greyDark,
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                  ),
                )),
            Expanded(
              flex: 1,
              child: Text(
                secondColumnValue,
                textAlign: TextAlign.right,
                style: TextStyle(
                  color: ColorsMyApp.greyDark,
                  fontWeight: FontWeight.normal,
                  fontSize: 14,
                ),
              ),
            ),
          ],
        ));
  }

  Widget _buildChilds(FoodItems foodItems, int index) {
    return Container(
      color: Colors.white70,
      padding: EdgeInsets.only(top: 2, bottom: 2, right: 4),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 3, left: 10),
              height: 8.0,
              width: 8.0,
              decoration: new BoxDecoration(
                color: foodItems.isVeg ? Colors.green : Colors.red,
                shape: BoxShape.circle,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 16),
              child: Text(
                foodItems.title,
                style: TextStyle(color: ColorsMyApp.greyDark, fontSize: 14),
              ),
            ),
          ]),
          Row(children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 30),
              child: Icon(
                IconsMyApp.rupee,
                size: 14,
                color: ColorsMyApp.greyDark,
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: EdgeInsets.only(top: 2),
                child: Text(
                  foodItems.price,
                  style: TextStyle(color: ColorsMyApp.greyDark, fontSize: 14),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Stack(
                children: <Widget>[
                  Visibility(
                    visible: false,
                    child: new InkWell(
                      onTap: () {
                        setState(() {});
                      },
                      child: Container(
                        width: 120,
                        height: 30,
                        padding: EdgeInsets.only(
                            top: 6, bottom: 6, left: 30, right: 30),
                        decoration: new BoxDecoration(
                          color: Colors.transparent,
                          border: Border.all(
                            color: ColorsMyApp.grey_border,
                            width: 1,
                          ),
                        ),
                        child: Center(
                            child: Text(
                          StringsMyApp.add,
                          style: TextStyle(color: ColorsMyApp.themeColor),
                        )),
                      ),
                    ),
                  ),
                  new Container(
                    width: 120,
                    height: 30,
                    decoration: new BoxDecoration(
                      color: Colors.transparent,
                      border: Border.all(
                        color: ColorsMyApp.grey_border,
                        width: 1,
                      ),
                    ),
                    child: Row(
                      children: <Widget>[
                        InkWell(
                            onTap: () {
                              setState(() {
                                if (foodItems.quantity >1) {
                                  foodItems.quantity--;
                                }
                              });
                            },
                            child: Padding(
                                padding: EdgeInsets.only(left: 6),
                                child: Icon(
                                  Icons.remove,
                                  size: 20,
                                  color: ColorsMyApp.greyDark,
                                ))),
                        Expanded(
                            child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  foodItems.quantity.toString(),
                                  style: TextStyle(
                                      color: ColorsMyApp.themeColor,
                                      fontSize: 16),
                                ))),
                        InkWell(
                            onTap: () {
                              setState(() {
                                if (foodItems.quantity <= 10) {
                                  foodItems.quantity++;
                                }
                              });
                            },
                            child: Padding(
                                padding: EdgeInsets.only(right: 6),
                                child: Icon(
                                  Icons.add,
                                  size: 20,
                                  color: ColorsMyApp.themeColor,
                                ))),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ]),
          index == (data.length - 1)
              ? Container(
                  margin: EdgeInsets.only(top: 4),
                )
              : Divider(),
        ],
      ),
    );
  }
}
