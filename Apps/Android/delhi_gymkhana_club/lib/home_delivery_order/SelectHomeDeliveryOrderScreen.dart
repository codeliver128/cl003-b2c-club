import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/IconsConstants.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils/NavigationRouteUtil.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class SelectHomeDeliveryOrderScreen extends StatefulWidget {
  @override
  _SelectHomeDeliveryOrderScreenState createState() =>
      _SelectHomeDeliveryOrderScreenState();
}

class _SelectHomeDeliveryOrderScreenState
    extends State<SelectHomeDeliveryOrderScreen> {
  List<FoodTypes> data = FoodTypes().getFoodTypes();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.home_delivery_order,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index) =>
                  EntryItem(data[index]),
              itemCount: data.length,
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding:
                  EdgeInsets.only(top: 10, bottom: 10, left: 16, right: 16),
              child: Button(
                buttonText: StringsMyApp.proceed,
                onButtonClickFunction: (BuildContext context) {
                  NavigationRouteUtil().navigateToScreen(
                      context, StringsMyApp.homedeliverybillandpayscreen);
                },
                buttonBackGroundColor: ColorsMyApp.themeColor,
                buttonTextColor: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// One entry in the multilevel list displayed by this app.
class FoodTypes {
  FoodTypes({@required title, @required foodItemsList})
      : _title = title,
        _foodItemsList = foodItemsList;

  String _title;
  List<FoodItems> _foodItemsList;

  String get title => _title;

  set title(String value) {
    _title = value;
  }

  getFoodTypes() {
    return [
      FoodTypes(title: "Starters", foodItemsList: [
        FoodItems(title: "Chilli Paneer (Dry)", price: "120", isVeg: true),
        FoodItems(title: "Veg Manchurain", price: "120", isVeg: true),
        FoodItems(
            title: "kesari Paneer Tikka (Dry)", price: "120", isVeg: true),
        FoodItems(title: "Chicken Tikka", price: "120", isVeg: false)
      ]),
      FoodTypes(title: "Main Course", foodItemsList: [
        FoodItems(title: "Chilli Paneer (Dry)", price: "120", isVeg: true),
        FoodItems(title: "Veg Manchurain", price: "120", isVeg: true),
        FoodItems(
            title: "kesari Paneer Tikka (Dry)", price: "120", isVeg: true),
        FoodItems(title: "Chicken Tikka", price: "120", isVeg: false)
      ]),
      FoodTypes(title: "Sweets", foodItemsList: [
        FoodItems(title: "Chilli Paneer (Dry)", price: "120", isVeg: true),
        FoodItems(title: "Veg Manchurain", price: "120", isVeg: true),
        FoodItems(
            title: "kesari Paneer Tikka (Dry)", price: "120", isVeg: true),
        FoodItems(title: "Chicken Tikka", price: "120", isVeg: false)
      ]),
      FoodTypes(title: "Chinese", foodItemsList: [
        FoodItems(title: "Chilli Paneer (Dry)", price: "120", isVeg: true),
        FoodItems(title: "Veg Manchurain", price: "120", isVeg: true),
        FoodItems(
            title: "kesari Paneer Tikka (Dry)", price: "120", isVeg: true),
        FoodItems(title: "Chicken Tikka", price: "120", isVeg: false)
      ]),
      FoodTypes(title: "Beverages", foodItemsList: [
        FoodItems(title: "Chilli Paneer (Dry)", price: "120", isVeg: true),
        FoodItems(title: "Veg Manchurain", price: "120", isVeg: true),
        FoodItems(
            title: "kesari Paneer Tikka (Dry)", price: "120", isVeg: true),
        FoodItems(title: "Chicken Tikka", price: "120", isVeg: false)
      ]),
      FoodTypes(title: "Thalis", foodItemsList: [
        FoodItems(title: "Chilli Paneer (Dry)", price: "120", isVeg: true),
        FoodItems(title: "Veg Manchurain", price: "120", isVeg: true),
        FoodItems(
            title: "kesari Paneer Tikka (Dry)", price: "120", isVeg: true),
        FoodItems(title: "Chicken Tikka", price: "120", isVeg: false)
      ]),
    ];
  }

  List<FoodItems> get foodItemsList => _foodItemsList;

  set foodItemsList(List<FoodItems> value) {
    _foodItemsList = value;
  }
}

class FoodItems {
  FoodItems({@required title, @required price, @required isVeg, quantity})
      : _title = title,
        _price = price,
        _isVeg = isVeg,
        _quantity = quantity;

  String _title;
  String _price;
  bool _isVeg;
  int _quantity;

  String get title => _title;

  set title(String value) {
    _title = value;
  }

  String get price => _price;

  set price(String value) {
    _price = value;
  }

  bool get isVeg => _isVeg;

  set isVeg(bool value) {
    _isVeg = value;
  }

  int get quantity => _quantity;

  set quantity(int value) {
    _quantity = value;
  }


}

class EntryItem extends StatelessWidget {
  const EntryItem(this.entry);

  final FoodTypes entry;

  Widget _buildTiles(FoodTypes root) {
    if (root.foodItemsList.isEmpty) return ListTile(title: Text(root.title));
    return Card(
        color: Colors.white,
        child: ExpansionTile(
          key: PageStorageKey<FoodTypes>(root),
          title: Text(root.title,
              style: TextStyle(
                  color: ColorsMyApp.themeColor,
                  fontSize: 18,
                  fontWeight: FontWeight.bold)),
          children: root.foodItemsList.map<Widget>(_buildChilds).toList(),
        ));
  }

  Widget _buildChilds(FoodItems foodItems) {
    return Container(
      color: Colors.white10,
      padding: EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 3, left: 10),
              height: 8.0,
              width: 8.0,
              decoration: new BoxDecoration(
                color: foodItems.isVeg ? Colors.green : Colors.red,
                shape: BoxShape.circle,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 16),
              child: Text(
                foodItems.title,
                style: TextStyle(color: ColorsMyApp.greyDark, fontSize: 15),
              ),
            ),
          ]),
          Row(children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 30),
              child: Icon(
                IconsMyApp.rupee,
                size: 15,
                color: ColorsMyApp.greyDark,
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: EdgeInsets.only(top: 2),
                child: Text(
                  foodItems.price,
                  style: TextStyle(color: ColorsMyApp.greyDark, fontSize: 15),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.centerRight,
                child: new Container(
                  padding:
                      EdgeInsets.only(top: 6, bottom: 6, left: 30, right: 30),
                  decoration: new BoxDecoration(
                    color: Colors.transparent,
                    border: Border.all(
                      color: ColorsMyApp.grey_border,
                      width: 1,
                    ),
                  ),
                  child: Center(
                      child: Text(
                    StringsMyApp.add,
                    style: TextStyle(color: ColorsMyApp.themeColor),
                  )),
                ),
              ),
            ),
          ]),
          Divider(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(entry);
  }
}
