class FoodServicesModel {
  String _id;
  String _foodServiceTypeName;

  FoodServicesModel({id, foodServiceTypeName})
      : _id = id,
        _foodServiceTypeName = foodServiceTypeName;

  String get foodServiceTypeName => _foodServiceTypeName;

  set foodServiceTypeName(String value) {
    _foodServiceTypeName = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  getFoodServicesTypes() {
    return [
      FoodServicesModel(id: "1", foodServiceTypeName: "Coffee Shop"),
      FoodServicesModel(id: "1", foodServiceTypeName: "Restaurant"),
      FoodServicesModel(id: "1", foodServiceTypeName: "Bar"),
    ];
  }
}
