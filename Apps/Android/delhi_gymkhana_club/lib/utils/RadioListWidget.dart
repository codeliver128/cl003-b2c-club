import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

typedef void StringCallback(String val);

class RadioListWidget extends StatelessWidget {
  List<String> listOfRadioValues;
  final StringCallback callback;
  String selectedRadioButton;

  RadioListWidget(
      {@required this.callback,
      @required this.listOfRadioValues,
      @required this.selectedRadioButton});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      // Let the ListView know how many items it needs to build.
      itemCount: listOfRadioValues.length,
      // Provide a builder function. This is where the magic happens.
      // Convert each item into a widget based on the type of item it is.
      itemBuilder: (context, index) {
        final item = listOfRadioValues[index];
        return Container(
          height: 28,
          child: InkWell(
            onTap: () {
              callback(item);
              print(item);
            },
            child: Row(
              children: <Widget>[
                Radio(
                    activeColor: ColorsMyApp.themeColor,
                    value: item,
                    groupValue: selectedRadioButton,
                    onChanged: (value) {
                      callback(value);
                      print(item);
                    }),
                Text(
                  item,
                  style: TextStyle(fontSize: 13, color: Colors.black87),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
