import 'package:delhi_gymkhana_club/constants/Constants.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:flutter/cupertino.dart';

class NavigationRouteUtil {
  navigateToScreen(BuildContext context, String title,
      {Object arguments, bool removePreviousScreens}) {
    String NAVIGATION_SCREEN;
    switch (title) {
      case StringsMyApp.login:
        NAVIGATION_SCREEN = LOGIN_SCREEN;
        break;

      case StringsMyApp.home:
        NAVIGATION_SCREEN = HOME_SCREEN;
        break;

      case StringsMyApp.membership_enquiry:
        NAVIGATION_SCREEN = MEMBERSHIP_ENQUIRY;
        break;

      case StringsMyApp.forgot_password:
        NAVIGATION_SCREEN = FORGOT_PASSWORD;
        break;

      case StringsMyApp.member_bill_payment:
        NAVIGATION_SCREEN = MEMBER_BILL_PAYMENT;
        break;

      case StringsMyApp.view_profile:
        NAVIGATION_SCREEN = VIEW_PROFILE_SCREEN;
        break;

      case StringsMyApp.events_calendar:
        NAVIGATION_SCREEN = EVENTS_CALENDAR;
        break;

      case StringsMyApp.important_notices:
        NAVIGATION_SCREEN = IMPORTANT_NOTICES_SCREEN;
        break;

      case StringsMyApp.transaction_history:
        NAVIGATION_SCREEN = TRANSACTION_HISTORY_SCREEN;
        break;

      case StringsMyApp.last_pos_transaction_details:
        NAVIGATION_SCREEN = POS_TRANSACTION_DETAIL_SCREEN;
        break;

      case StringsMyApp.last_room_bookings:
        NAVIGATION_SCREEN = ROOM_BOOKING_DETAIL_SCREEN;
        break;

      case StringsMyApp.last_party_venue_bookings:
        NAVIGATION_SCREEN = PARTY_VENUE_BOOKING_DETAIL_SCREEN;
        break;

      case StringsMyApp.last_payment_details:
        NAVIGATION_SCREEN = PAYMENT_DETAIL_SCREEN;
        break;

      case StringsMyApp.gc_minutes:
      case StringsMyApp.notice_for_egm:
        NAVIGATION_SCREEN = IMPORTANT_NOTICE_TYPES_LIST;
        break;

      case StringsMyApp.online_reservation:
        NAVIGATION_SCREEN = ONLINE_RESERVATIONS_SCREEN;
        break;

      case StringsMyApp.sports_Slot_reservation:
        NAVIGATION_SCREEN = SPORT_SLOT_RESERVATION_SCREEN;
        break;

      case StringsMyApp.books_reservation:
        NAVIGATION_SCREEN = BOOKS_RESERVATION_SCREEN;
        break;

      case StringsMyApp.book_detail:
        NAVIGATION_SCREEN = BOOK_DETAIL_SCREEN;
        break;

      case StringsMyApp.room_reservation:
        NAVIGATION_SCREEN = ROOM_RESERVATIONS_SCREEN;
        break;

      case StringsMyApp.room_booking:
        NAVIGATION_SCREEN = ROOM_BOOKING_SCREEN;
        break;

      case StringsMyApp.room_bill_detail_title:
        NAVIGATION_SCREEN = ROOM_RESERVATION_BILLING_SCREEN;
        break;

      case StringsMyApp.party_venue_reservation:
        NAVIGATION_SCREEN = PARTY_VENUE_AVAILABLLITY_SCREEN;
        break;

      case StringsMyApp.member_directory:
        NAVIGATION_SCREEN = MEMBERS_DIRECTORY_SCREEN;
        break;

      case StringsMyApp.post_your_feedback:
        NAVIGATION_SCREEN = FEEDBACK_SCREEN;
        break;

      case StringsMyApp.change_password:
        NAVIGATION_SCREEN = CHANGE_PASSWORD_SCREEN;
        break;

      case StringsMyApp.home_delivery_order:
        NAVIGATION_SCREEN = HOME_DELIVERY_ORDER_SCREEN;
        break;

      case StringsMyApp.select_home_delivery_order_screen:
        NAVIGATION_SCREEN = SELECT_HOMEDELIVERY_ORDER_SCREEN;
        break;

      case StringsMyApp.add_venue_screen:
        NAVIGATION_SCREEN = ADD_VENUE_SCREEN;
        break;

      case StringsMyApp.party_venue_billing_screen:
        NAVIGATION_SCREEN = PARTY_VENUE_BILL_DETAIL_SCREEN;
        break;

      case StringsMyApp.homedeliverybillandpayscreen:
        NAVIGATION_SCREEN = HOME_DELIVERY_BILLANDPAY_SCREEN;
        break;



      default:
        NAVIGATION_SCREEN = FORGOT_PASSWORD;
        break;
    }
    arguments != null
        ? removePreviousScreens != null && removePreviousScreens
            ? Navigator.pushReplacementNamed(context, NAVIGATION_SCREEN,
                arguments: arguments)
            : Navigator.pushNamed(context, NAVIGATION_SCREEN,
                arguments: arguments)
        : removePreviousScreens != null && removePreviousScreens
            ? Navigator.pushReplacementNamed(context, NAVIGATION_SCREEN)
            : Navigator.pushNamed(context, NAVIGATION_SCREEN);
  }
}
