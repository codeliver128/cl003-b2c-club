import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/transaction_history/models/PosTransactionModel.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:delhi_gymkhana_club/utils_widget/NoDataWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';


List<PosTransactionModel> listPosTransactionModels;

class RoomBookingDetailScreen extends StatefulWidget {
  @override
  _RoomBookingDetailScreenState createState() => _RoomBookingDetailScreenState();
}

class _RoomBookingDetailScreenState extends State<RoomBookingDetailScreen> {
  @override
  Widget build(BuildContext context) {
    //var listImportantNotices;
    listPosTransactionModels = [
      PosTransactionModel(
          amount: "1000.00", location: "Coffe Shop", billNumber: "123456"),
      PosTransactionModel(
          amount: "1023.00", location: "Bar", billNumber: "123467"),
      PosTransactionModel(
          amount: "13240.00", location: "Restaurant", billNumber: "122456"),
      PosTransactionModel(
          amount: "230.00", location: "PVR", billNumber: "098848"),
      PosTransactionModel(
          amount: "560.00", location: "Food Court", billNumber: "948484"),
      PosTransactionModel(
          amount: "9000.00", location: "Bar", billNumber: "126756"),
      PosTransactionModel(
          amount: "160.00", location: "Coffe Shop", billNumber: "123986"),
      PosTransactionModel(
          amount: "1800.00", location: "Coffe Shop", billNumber: "115356"),
      PosTransactionModel(
          amount: "1760.00", location: "Coffe Shop", billNumber: "185456"),
      PosTransactionModel(
          amount: "8700.00", location: "Coffe Shop", billNumber: "523456"),
      PosTransactionModel(
          amount: "1350.00", location: "Coffe Shop", billNumber: "168456"),
      PosTransactionModel(
          amount: "1380.00", location: "Coffe Shop", billNumber: "193656"),
    ];

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          actions: <Widget>[HomeIconToolbar()],
          iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
          title: new Text(
            StringsMyApp.last_room_bookings,
            style: TextStyle(color: ColorsMyApp.VividOrange),
          ),
          backgroundColor: Colors.white,
          elevation: 4,
        ),
        body: listPosTransactionModels == null ||
            listPosTransactionModels.length == 0
            ? NoDataWidget()
            : TransactionDataWidget());
  }
}

class TransactionDataWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: ListView.builder(
        itemCount: listPosTransactionModels.length,
        itemBuilder: (context, position) {
          return _getListRow(context, position);
        },
      ),
    );
  }

  _getListRow(BuildContext context, int position) {
    return Card(
      elevation: 4,
      color: ColorsMyApp.navigationBackGround,
      child: InkWell(
        onTap: () {
          //  _askUser(position);
        },
        child: Padding(
          padding: EdgeInsets.only(top: 16, bottom: 16, left: 16, right: 16),
          child: Column(children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    StringsMyApp.location,
                    style: TextStyle(color: Colors.black),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      listPosTransactionModels.elementAt(position).location,
                      style: TextStyle(color: ColorsMyApp.greyDark),
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 6),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    StringsMyApp.bill_number,
                    style: TextStyle(color: Colors.black),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      listPosTransactionModels.elementAt(position).billNumber,
                      style: TextStyle(color: ColorsMyApp.greyDark),
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 6),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    StringsMyApp.amount,
                    style: TextStyle(color: Colors.black),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      listPosTransactionModels.elementAt(position).amount,
                      style: TextStyle(color: ColorsMyApp.greyDark),
                    ),
                  ),
                ),
              ],
            )
          ]),
        ),
      ),
    );
  }
}
