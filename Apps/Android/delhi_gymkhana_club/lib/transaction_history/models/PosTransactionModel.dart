class PosTransactionModel {
  String _location;
  String _billNumber;
  String _amount;

  PosTransactionModel({location, billNumber, amount})
      : _location = location,
        _billNumber = billNumber,
        _amount = amount;

  String get amount => _amount;

  set amount(String value) {
    _amount = value;
  }

  String get billNumber => _billNumber;

  set billNumber(String value) {
    _billNumber = value;
  }

  String get location => _location;

  set location(String value) {
    _location = value;
  }
}
