import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/home/HomeScreenHeader.dart';
import 'package:delhi_gymkhana_club/utils/NavigationRouteUtil.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class TransactionHistoryScreen extends StatefulWidget {
  @override
  _TransactionHistoryScreenState createState() =>
      _TransactionHistoryScreenState();
}

class _TransactionHistoryScreenState extends State<TransactionHistoryScreen> {
  List<Map<String, Object>> listImportantNotices =
      StringsMyApp.list_transaction_history;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.transaction_history,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 4, bottom: 16),
        child: Column(
          children: <Widget>[
            HomeScreenHeader(),
            Expanded(
                child: Container(
              margin: EdgeInsets.only(left: 10, right: 10),
              child: ListView.builder(
                itemCount: listImportantNotices.length,
                itemBuilder: (context, position) {
                  return _getListRow(context, position);
                },
              ),
            )),
          ],
        ),
      ),
    );
  }

  _getListRow(BuildContext context, int position) {
    return Card(
      elevation: 4,
      color: ColorsMyApp.themeColor,
      child: InkWell(
        onTap: () {
          NavigationRouteUtil().navigateToScreen(
              context, listImportantNotices.elementAt(position)['title']);
        },
        child: Padding(
          padding: EdgeInsets.only(
            top: 16,
            bottom: 16,
          ),
          child: Center(
            child: Column(
              children: <Widget>[
                Icon(
                  listImportantNotices.elementAt(position)['icon'],
                  color: Colors.black,
                  size: 30,
                ),
                Text(
                  listImportantNotices.elementAt(position)['title'],
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
