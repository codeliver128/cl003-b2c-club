import 'package:delhi_gymkhana_club/constants/Constants.dart';
import 'package:delhi_gymkhana_club/utils/NavigationRouteUtil.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';

import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/constants/ImageAssetsConstants.dart';
import 'package:delhi_gymkhana_club/utils_widget/InputBoxRectangular.dart';
import 'package:toast/toast.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DecoratedBox(
      position: DecorationPosition.background,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(ImageAssetsConstants.login_background_image),
          fit: BoxFit.cover,
        ),
      ),
      child: Padding(
        padding: EdgeInsets.only(left: 24, right: 24, bottom: 10),
        child: SafeArea(
          child: Column(children: <Widget>[
            Expanded(
                child: Center(
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Image(
                      image: AssetImage(ImageAssetsConstants.app_logo),
                    ),
                    InputBoxRectangular(
                      hintText: StringsMyApp.user_name,
                      isPasswordField: false,
                      hintColor: ColorsMyApp.greyDark,
                      textColor: Colors.black,
                    ),
                    InputBoxRectangular(
                      hintText: StringsMyApp.password,
                      isPasswordField: true,
                      boxBorderColor: ColorsMyApp.themeColor,
                      hintColor: ColorsMyApp.greyDark,
                      textColor: Colors.black,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Button(
                        buttonText: StringsMyApp.login,
                        onButtonClickFunction: (BuildContext context) {
                          NavigationRouteUtil().navigateToScreen(
                              context, StringsMyApp.home,
                              removePreviousScreens: true);
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Button(
                        buttonBackGroundColor: ColorsMyApp.white_65,
                        buttonText: StringsMyApp.forgot_password,
                        onButtonClickFunction: (BuildContext context) {
                          NavigationRouteUtil().navigateToScreen(
                              context, StringsMyApp.forgot_password);
                        },
                        buttonTextColor: Colors.black54,
                      ),
                    ),
                  ],
                ),
              ),
            )),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(top: 10),
                child: Button(
                  buttonText: StringsMyApp.new_membership_enquiry,
                  onButtonClickFunction: (BuildContext context) {
                    NavigationRouteUtil().navigateToScreen(
                        context, StringsMyApp.membership_enquiry);
                  },
                  buttonBackGroundColor: ColorsMyApp.black_low,
                  buttonTextColor: Colors.white,
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
