import 'dart:ui';

class ColorsMyApp {
  static const PrimaryColor = Color(0xFF808080);
  static const PrimaryAssentColor = Color(0xFF808080);
  static const PrimaryDarkColor = Color(0xFF808080);
  static const ErroColor = Color(0xFF808080);

  static const VividOrange = Color(0xFFFFB31A);

  static const themeColor = Color(0xFFFFB31A);

  static const greyDark = Color(0xFF707070);
  static const buttonTextColor = Color(0xFF000000);

  static const hamburgerOrBackIcon = Color(0x8A000000);
  static const editBoxBorderColor = Color(0xFFE1E1E1);
  static const navigationBackGround = Color(0xFFF1F1F1);
  static const grey_21 = Color(0x36DBD8D8);
  static const grey_border = Color(0xFFDEDCDC);

  static const grey_54 = Color(0x8A000000);
  static const white_54 = Color(0x8AFFFFFF);
  static const white_65 = Color(0xA6FFFFFF);
  static const themeColor_light = Color(0xFFFCEBC7);

  static const available_color = themeColor_light;
  static const unavailable_color = grey_border;
  static const selected_slot_box_color = themeColor;

  static const button_disabled = grey_border;
  static const button_enabled = themeColor;

  static const green = Color(0xFF0B822F);
  static const red = Color(0xFFFF0909);
  static const black_low = Color(0xFF504E4E);
  static const orange_low = Color(0xFFFFF7E6);
  static const grey_10 = Color(0x5CFAFAFA);


}
