import 'package:flutter/material.dart';


class ImageAssetsConstants{

  static const String app_logo='assets/images/app_logo.png';
  static const String login_background_image='assets/images/login_back.png';
  static const String profile_banner_image='assets/images/banner_image.jpg';
  static const String splash_back_image='assets/images/splash_back.jpg';


}