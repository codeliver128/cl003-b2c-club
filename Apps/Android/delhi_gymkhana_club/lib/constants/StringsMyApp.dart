import 'package:flutter/material.dart';

class StringsMyApp {
  static const List<String> payment_mode_list = [
    net_banking,
    credit_debit_card,
    amex
  ];
  static const List<String> profile_tab_title_list = [self, spouse, dependants];

  static const List<String> important_notices_list = [
    gc_minutes,
    notice_for_egm
  ];

  static var list_transaction_history = [
    {
      "icon": Icons.account_balance_wallet,
      "title": last_pos_transaction_details
    },
    {"icon": Icons.room_service, "title": last_room_bookings},
    {"icon": Icons.hotel, "title": last_party_venue_bookings},
    {"icon": Icons.payment, "title": last_payment_details}
  ];

  static var list_online_reservation = [
    {"icon": Icons.laptop_chromebook, "title": room_reservation},
    {"icon": Icons.local_laundry_service, "title": party_venue_reservation},
    {"icon": Icons.local_convenience_store, "title": books_reservation},
    {"icon": Icons.subscriptions, "title": sports_Slot_reservation}
  ];

  static const String room_reservation = "Room Reservation";
  static const String party_venue_reservation = "Party Venue Reservation";
  static const String books_reservation = "Books Reservation";
  static const String sports_Slot_reservation = "Sports Slot Reservation";

  static const String last_pos_transaction_details =
      "Last 10 POS transaction details";
  static const String last_room_bookings = "Last 5 Room Bookings";
  static const String last_party_venue_bookings = "Last 5 Party Venue Bookings";
  static const String last_payment_details = "Last 5 Payment Details";

  static const String home = "Home";

  static const String self = "Self";
  static const String spouse = "Spouse";
  static const String dependants = "Dependants";
  static const String login = "Login";
  static const String user_name = "Mem_ID/Club_ID";
  static const String password = "Password";
  static const String forgot_password = "Forgot Password";
  static const String membership_enquiry = "Membership Enquiry";
  static const String new_membership_enquiry = "New Membership Enquiry";
  static const String login_info_content =
      "Username: MemberID\nPassword: Last 4 digits of MemberID followed by Last 4 character of Last name";
  static const String member_id_colon = "Mem_ID:";

  static const String registration_id = "Reg. ID:";
  static const String registration_date_colon = "Reg. Date:";
  static const String membership_date_colon = "Membership Date:";

  static const String dob_colon = "DoB:";
  static const String mobile_colon = "Mobile:";
  static const String status_colon = "Status:";
  static const String active = "Active";
  static const String inactive = "Inactive";
  static const String member_bill_payment = "Member Bill Payment";
  static const String online_reservation = "Online Reservations";
  static const String home_delivery_order = "Home Delivery Orders";
  static const String transaction_history = "Transactions History";
  static const String events_calendar = "Events Calendar";
  static const String important_notices = "Important Notices";
  static const String members_directory = "Member's Directory";
  static const String post_your_feedback = "Post Your Feedback";
  static const String party_venue_booking = "Party Venue Booking";
  static const String room_booking = "Room Booking";
  static const String online_payment_history = "Online Payment History";
  static const String card_charge_details = "Card Charge Details";
  static const String view_transactions = "View Transactions";
  static const String offline_payments_history = "Offline Payments History";
  static const String gc_minutes = "GC Minutes";
  static const String notice_for_egm = "Notice for EGM 2019";
  static const String submit = "Submit";
  static const String memberID = "MemberID";
  static const String name = "Name";
  static const String mobile_no = "Mobile No";
  static const String email_id = "Email ID";
  static const String relation_colon = "Relation:";
  static const String message = "Message";
  static const String view_profile = "View Profile";
  static const String member_directory = "Member's Directory";
  static const String change_password = "Change Password";
  static const String log_out = "Log Out";
  static const String profile = "Profile";
  static const String address_colon = "Address:";
  static const String email_id_colon = "Email Id:";
  static const String previous_bill = "Previous Bill";
  static const String current_bill = "Current Bill";
  static const String view_bill = "View Bill";
  static const String total_payment_due_colon = "Total payment due: ";
  static const String bill_no = "Bill No.";
  static const String bill_date = "Bill Date";
  static const String bill_month = "Bill Month";
  static const String staus = "Status";
  static const String due_date = "Due Date";
  static const String next = "Next";
  static const String member_bill_detail = "Member Bill Detail";
  static const String opening_balance_A = "Opening Balance(A)";
  static const String payment_received_B = "Payment Received(B)";
  static const String arrears_c = "Arrears(C=A-B)";
  static const String current_bill_amt_D = "Current Bill Amt(D)";
  static const String amt_payable_E = "Amt Payable(E=C+D)";
  static const String pay_extra_amt = "Pay Extra Amount, if Desired";
  static const String note_colon = "Note:";
  static const String payment_note_1 =
      "Payment received between 20-May-2019 and 21-May-2019 is adjusted in this Bill.";
  static const String payment_note_2 =
      "Please pay on or before the due date to avoid penalty interest @10% pm.";
  static const String proceed_payment = "Proceed For Payment";
  static const String member_id = "Member ID";
  static const String member_name = "Member Name";
  static const String bill_due_date = "Bill Due Date";
  static const String bill_amount = "Bill Amount";
  static const String extra_amount = "Extra Amount";
  static const String final_payable = "Final Payable";
  static const String mode_of_payment = "MODE OF PAYMENT";
  static const String net_banking = "NET BANKING";
  static const String credit_debit_card = "CREDIT / DEBIT CARD";
  static const String amex = "AMEX";
  static const String agree_with = "I agree with the ";
  static const String terms_and_conditions = "Terms & Conditions.";
  static const String select_payment_option_msg =
      "Please Select Payment Option Mentioned Above.";
  static const String pay_now = "Pay Now";
  static const String pls_select_payment_option =
      "Please select Payment Option.";
  static const String accept_terms_conditions =
      "Please accept Terms & Conditions.";
  static const String edit_profile = "Edit Profile";
  static const String request_update = "Request to Update";
  static const String edit = "Edit";
  static const String ok = "Ok";
  static const String location = "Location";
  static const String bill_number = "Bill Number";
  static const String amount = "Amount";
  static const String available = "Available";
  static const String unavailable = "Unavailable";
  static const String reserve = "Reserve";
  static const String select_sports = "Select Sports";
  static const String select_date = "Select Date";
  static const String book_detail = "Book Detail";
  static const String author_title = "Author / Title";
  static const String request = "Request";
  static const String category = "Category";
  static const String checkin_date = "Check In Date";
  static const String checkout_date = "Check Out Date";
  static const String online_room_booking = "Online Room Booking is allowed:";
  static const String roombooking_note1 =
      "For a maximum No. of 2 Rooms for a maximum period of 15 Nights.";
  static const String roombooking_note2 =
      "Upto 90 days in advance from the Current Date.";
  static const String roombooking_note3 =
      "The Check-In Date must be 2 days after the Current Date.";
  static const String select_check_in_date =
      "Please select check-in date first";
  static const String total_nights = "Total Nights";
  static const String booking_for = "Booking For";
  static const String room_category = "Room Category";
  static const String tarrif_per_night = "Tarrif (per night)";
  static const String rooms_available = "Rooms Available";
  static const String rooms_to_be_booked = "Rooms To Be Booked";
  static const String select = "Select";
  static const String room_bill_detail_title = "room_bill_detail";
  static const String relation = "Relation";
  static const String room_charges = "Room Charges";
  static const String gst_percent = "GST %";
  static const String gst_amount = "GST Amount";
  static const String booked_room = "Booked Room";
  static const String total_amount = "Total Amount";
  static const String book_now = "Book Now";
  static const String function_date = "Function Date";
  static const String select_session = "Select Session";
  static const String select_venue = "Select Venue";
  static const String venue = "Venue";
  static const String session = "Session";
  static const String check_availability = "Check Availability";
  static const String note_party_venue_header =
      "Online Party Venue Booking is allowed:";
  static const String note_party_venue_note1 =
      "Upto 30 days in advance from the Current Date.";
  static const String note_party_venue_note2 =
      "The Function Date must be 2 days after the Current Date.";
  static const String select_criteria = "Select Criteria";
  static const String enter_value = "Enter Value";
  static const String search = "Search";
  static const String reset = "Reset";
  static const String total_records = "Total Records - ";
  static const String feedback = "Feedback";
  static const String feedback_health_club =
      "Feedback Type- Health Club Feedback";

  static const String feedback_suggesstion_msg =
      "Club is committed to provide you best of the facilities/ amenities possible. We greatly appreciate if you take few minutes to let us know how we are doing by filling up questions below and also suggestions for improvement.";

  static const String select_one = "Select One";
  static const String old_password = "Old Password";
  static const String new_password = "New Password";
  static const String confirm_password = "Confirm Password";

  static const String logout_msg = "Are you sure you want to log out?";
  static const String cancel = "Cancel";
  static const String confirm = "Confirm";
  static const String date = "Date";
  static const String select_home_delivery_order_screen =
      "select home delivery order screen";
  static const String add = "Add";
  static const String no_of_rooms_avlbl = "No. of Rooms Available ";
  static const String enter_rooms_to_be_booked =
      "Enter No. of Rooms To Be Booked";
  static const String add_venue_screen = "add_venue_screen";
  static const String party_venue_billing_screen = "party_venue_billing_screen";
  static const String homedeliverybillandpayscreen =
      "homedelivery_billandpay_screen";
  static const String remarks_if_any = "Remarks (If Any)";
  static const String remarks = "Remarks";
  static const String venue_charges = "Venue Charges";
  static const String filter_by = "Filter By";
  static const String select_month = "Select Month";
  static const String subject = "Subject";
  static const String enter_subject_hint = "Enter subject...";
  static const String comment = "Comment";
  static const String enter_comment_hint = "Enter comment...";
  static const String select_feedback = "Select Feedback";
  static const String enter_subject = "Enter Subject";
  static const String enter_comment = "Enter Comment";
  static const String bill_details = "Bill Details";
  static const String proceed = "Proceed";
  static const String rupee_symbol = "₹";
}
