import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class ImportantNoticeTypesList extends StatefulWidget {
  @override
  _ImportantNoticeTypesListState createState() =>
      _ImportantNoticeTypesListState();
}

class _ImportantNoticeTypesListState extends State<ImportantNoticeTypesList> {

  List<String> listImportantNotices;
  var screenTitle;

  @override
  Widget build(BuildContext context) {
    //var listImportantNotices;
    screenTitle = ModalRoute.of(context).settings.arguments;


    if(screenTitle==StringsMyApp.gc_minutes){
      listImportantNotices = [
        "GC-minutes - 06 JANUARY 2019",
        "GC-minutes - 06 FEBRUARY 2019",
        "GC-minutes - 06 MARCH 2019",
        "GC-minutes - 06 APRIL 2019",
        "GC-minutes - 06 MAY 2019",
        "GC-minutes - 06 JUNE 2019",
        "GC-minutes - 06 JULY 2019",
        "GC-minutes - 06 AUGUST 2019",
        "GC-minutes - 06 SEPTEMBER 2019",
        "GC-minutes - 06 OCTOBER 2019",
        "GC-minutes - 06 NOVEMBER 2019",
        "GC-minutes - 06 DECEMBER 2019"
      ];
    }else{
      listImportantNotices = [
        "EGM -  06 JANUARY 2019",
        "EGM -  06 FEBRUARY 2019",
        "EGM -  06 MARCH 2019",
        "EGM -  06 APRIL 2019",
        "EGM -  06 MAY 2019",
        "EGM -  06 JUNE 2019",
        "EGM -  06 JULY 2019",
        "EGM -  06 AUGUST 2019",
        "EGM -  06 SEPTEMBER 2019",
        "EGM -  06 OCTOBER 2019",
        "EGM -  06 NOVEMBER 2019",
        "EGM -  06 DECEMBER 2019"
      ];
    }


    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          screenTitle,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        margin: EdgeInsets.only(left: 16,right: 16),
        padding: EdgeInsets.only(top: 10),
        child: ListView.builder(
          itemCount: listImportantNotices.length,
          itemBuilder: (context, position) {
            return _getListRow(context, position);
          },
        ),
      ),
    );
  }

  _getListRow(BuildContext context, int position) {
    return Card(
      elevation: 4,
      color: ColorsMyApp.greyDark,
      child: InkWell(
        onTap: () {
          //  _askUser(position);
        },
        child: Padding(
          padding: EdgeInsets.only(top: 16, bottom: 16, left: 16, right: 16),
          child: Row(children: <Widget>[
            Text(
              listImportantNotices[position],
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.centerRight,
                child: Icon(
                  Icons.arrow_downward,
                  color: ColorsMyApp.white_54,
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
