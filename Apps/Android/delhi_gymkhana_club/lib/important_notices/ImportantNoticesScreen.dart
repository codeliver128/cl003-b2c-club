import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils/NavigationRouteUtil.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class ImportantNoticesScreen extends StatefulWidget {
  @override
  _ImportantNoticesScreenState createState() => _ImportantNoticesScreenState();
}

class _ImportantNoticesScreenState extends State<ImportantNoticesScreen> {
  List<String> listImportantNotices = StringsMyApp.important_notices_list;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.important_notices,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        margin: EdgeInsets.all(16),
        child: ListView.builder(
          itemCount: listImportantNotices.length,
          itemBuilder: (context, position) {
            return _getListRow(context, position);
          },
        ),
      ),
    );
  }

  _getListRow(BuildContext context, int position) {
    return Card(
      elevation: 4,
      color: ColorsMyApp.themeColor,
      child: InkWell(
        onTap: () {
          NavigationRouteUtil().navigateToScreen(
              context, listImportantNotices[position],
              arguments: listImportantNotices[position]);
        },
        child: Padding(
          padding: EdgeInsets.only(top: 26, bottom: 26),
          child: Center(
            child: Text(
              listImportantNotices[position],
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ),
        ),
      ),
    );
  }
}
