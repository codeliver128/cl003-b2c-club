import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/profile/SelfTabScreen.dart';
import 'package:delhi_gymkhana_club/profile/SpouseTabScreen.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:delhi_gymkhana_club/constants/Constants.dart';
import 'package:toast/toast.dart';

import 'DependantTabScreen.dart';

class ViewProfileScreen extends StatefulWidget {
  @override
  _ViewProfileScreenState createState() => _ViewProfileScreenState();
}

class _ViewProfileScreenState extends State<ViewProfileScreen>
    with SingleTickerProviderStateMixin {
  String name = "Laxman Singh";
  String img_url = "https://i.imgur.com/BoN9kdC.png";


/*
  Info to be added on profile screen,
  Primary member - Registration ID and Date & Membership ID and Date
  Spouse & Dependent - Primary Member Name, Relation , Email , Membership Date, Multi Dependent Handling

  */

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, initialIndex: 0, length: 3);
    _tabController.addListener(() {
      print("changing");
        if (_tabController.indexIsChanging||_tabController.previousIndex!=_tabController.index) {
          setState(() {
            switch (_tabController.index) {
              case 0:
                img_url = "https://i.imgur.com/BoN9kdC.png";
                name = "Laxman Singh";
                break;

              case 1:
                img_url =
                    "https://images.unsplash.com/photo-1529626455594-4ff0802cfb7e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80";
                name = "Simona Singh";
                break;

              case 2:
                img_url =
                    "https://media.gq.com/photos/56d75aedbeb4a66323eb9cd6/master/w_800%2Cc_limit/4-courtesy-of-The-Lions.jpg";
                name = "Arjun Singh";
                break;
            }
          });
        }

      print(_tabController.index.toString());
      });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.profile,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Column(children: <Widget>[
        Card(
          elevation: 2,
          color: Colors.white,
          child: Column(children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            Row(children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 26),
              ),
              new Container(
                width: 75.0,
                height: 75.0,
                decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 4.0,
                      // has the effect of softening the shadow
                      spreadRadius: 1.0,
                      // has the effect of extending the shadow
                      offset: Offset(
                        1.0, // horizontal, move right 10
                        3.0, // vertical, move down 10
                      ),
                    )
                  ],
                  border: Border.all(
                    width: 3,
                    color: Colors.white,
                  ),
                  image: new DecorationImage(
                    fit: BoxFit.fill,
                    image: new NetworkImage(img_url),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 22),
              ),
              Text(name,
                  style: TextStyle(
                      color: ColorsMyApp.themeColor,
                      fontSize: 18,
                      fontWeight: FontWeight.bold))
            ]),
            TabBar(
              indicatorColor: ColorsMyApp.themeColor,
              indicatorWeight: 5,
              labelColor: ColorsMyApp.themeColor,
              unselectedLabelColor: Colors.black,
              controller: _tabController,
              tabs: [
                Tab(
                  child: Text(
                    "Self",
                    style: TextStyle(fontSize: 15),
                  ),
                ),
                Tab(
                  child: Text(
                    "Spouse",
                    style: TextStyle(fontSize: 15),
                  ),
                ),
                Tab(
                  child: Text(
                    "Dependant",
                    style: TextStyle(fontSize: 15),
                  ),
                ),
              ],
            ),
          ]),
        ),
        Expanded(
          child: Container(
              margin: EdgeInsets.only(left: 10, right: 10, top: 16,bottom: 5),
              child: TabBarView(
                controller: _tabController,
                children: <Widget>[
                  SelfTabScreen(),
                  SpouseTabScreen(),
                  DependantTabScreen(),
                ],
              )),
        ),
      ]),
    );
  }


}
