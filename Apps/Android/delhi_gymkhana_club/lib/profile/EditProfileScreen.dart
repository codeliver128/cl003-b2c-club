import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:delhi_gymkhana_club/utils_widget/InputBoxRectangular.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class EditProfileScreen extends StatefulWidget {
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.edit_profile,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.only(top: 16, left: 16, right: 16),
        child: Column(
          children: <Widget>[
            InputBoxRectangular(
              hintText: StringsMyApp.mobile_no,
              isPasswordField: false,
              hintColor: ColorsMyApp.greyDark,
              boxBorderColor: ColorsMyApp.editBoxBorderColor,
            ),
            InputBoxRectangular(
              hintText: StringsMyApp.email_id,
              isPasswordField: false,
              hintColor: ColorsMyApp.greyDark,
              boxBorderColor: ColorsMyApp.editBoxBorderColor,
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Button(
                buttonText: StringsMyApp.request_update,
                onButtonClickFunction: _callMembershipEnquiryApi,
              ),
            )
          ],
        ),
      ),
    );
  }

  void _callMembershipEnquiryApi() {}
}
