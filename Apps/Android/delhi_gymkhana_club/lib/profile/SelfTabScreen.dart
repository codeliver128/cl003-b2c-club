import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/Constants.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class SelfTabScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(child:Column(
      children: <Widget>[
        _getTableRow(StringsMyApp.member_id_colon, "P-6564"),
        _getTableRow(StringsMyApp.membership_date_colon, "10 Aug 2019"),
        _getTableRow(StringsMyApp.registration_id, "P-65345"),
        _getTableRow(StringsMyApp.registration_date_colon, "1 Aug 2019"),
        _getTableRow(StringsMyApp.dob_colon, "17-July-1965"),
        _getTableRow(StringsMyApp.mobile_colon, "8065464456"),
        _getTableRow(StringsMyApp.email_id, "laxmanss@gmail.com"),
        _getTableRow(StringsMyApp.status_colon, "Active"),
        Expanded(
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(top: 10),
              child: Button(
                buttonText: StringsMyApp.edit,
                onButtonClickFunction: _clickButton,
                buttonTextColor: Colors.white,
              ),
            ),
          ),
        )
      ],
    ));
  }


  _getTableRow(String firstColumnTitle, secondColumnValue) {
    return Container(
        padding: EdgeInsets.only(top: 12),
        child: Row(
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Text(
                  firstColumnTitle,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                  ),
                )),
            Expanded(
              flex: 1,
              child: Text(
                secondColumnValue,
                textAlign: TextAlign.right,
                style: TextStyle(
                  color: ColorsMyApp.greyDark,
                  fontWeight: FontWeight.normal,
                  fontSize: 14,
                ),
              ),
            ),
          ],
        ));
  }

  _clickButton(BuildContext context) {
    Navigator.pushNamed(context, EDIT_PROFILE_SCREEN);
  }
}
