import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/Constants.dart';
import 'package:delhi_gymkhana_club/utils/NavigationRouteUtil.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';

class HomeGridTiles extends StatelessWidget {
  final IconData icon;
  final String title;

  const HomeGridTiles({this.icon, this.title});

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle =
        TextStyle(backgroundColor: ColorsMyApp.VividOrange);
    return Card(
      elevation: 4,
      color: ColorsMyApp.VividOrange,
      child: new InkWell(
        child: Padding(
          padding: EdgeInsets.only(top: 16, bottom: 16),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(icon, size: 30.0, color: textStyle.color),
                Text(title, style: TextStyle(color: Colors.white)),
              ],
            ),
          ),
        ),
        onTap: () {
          NavigationRouteUtil().navigateToScreen(context, title);
        },
      ),
    );
  }
}
