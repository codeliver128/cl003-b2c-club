import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class HomeScreenHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        elevation: 5,
        child: Padding(
          padding: EdgeInsets.only(top: 16, left: 2, right: 4, bottom: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Column(
                  children: <Widget>[
                    new Container(
                      width: 90.0,
                      height: 90.0,
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 4.0,
                            // has the effect of softening the shadow
                            spreadRadius: 1.0,
                            // has the effect of extending the shadow
                            offset: Offset(
                              1.0, // horizontal, move right 10
                              3.0, // vertical, move down 10
                            ),
                          )
                        ],
                        border: Border.all(
                          width: 4,
                          color: Colors.white,
                        ),
                        image: new DecorationImage(
                          fit: BoxFit.fill,
                          image: new NetworkImage(
                              "https://i.imgur.com/BoN9kdC.png"),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Text(
                        "Laxman Singh",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: ColorsMyApp.VividOrange,
                          fontSize: 15,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Padding(
                  padding: EdgeInsets.only(top: 10, left: 10),
                  child: Column(
                    children: <Widget>[
                      _getTableRow(StringsMyApp.member_id_colon, "P-6564"),
                      _getTableRow(StringsMyApp.dob_colon, "17-July-1965"),
                      _getTableRow(StringsMyApp.mobile_colon, "8065464456"),
                      _getTableRow(StringsMyApp.status_colon, "Active"),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _getTableRow(String firstColumnTitle, secondColumnValue) {
    return Container(
        padding: EdgeInsets.only(top: 6),
        child: Row(
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Text(
                  firstColumnTitle,
                  style: TextStyle(
                    color: ColorsMyApp.greyDark,
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                )),
            Expanded(
              flex: 1,
              child: Text(
                secondColumnValue,
                style: TextStyle(
                  color: ColorsMyApp.greyDark,
                  fontWeight: FontWeight.normal,
                  fontSize: 15,
                ),
              ),
            ),
          ],
        ));
  }
}
