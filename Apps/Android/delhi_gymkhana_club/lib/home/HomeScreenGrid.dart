import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'HomeGridTiles.dart';

class HomeScreenGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.only(top: 3.0),
        child: new StaggeredGridView.count(
          crossAxisCount: 2,
          staggeredTiles: _staggeredTiles,
          children: _tiles,
          mainAxisSpacing: 1.0,
          crossAxisSpacing: 1.0,
          padding: const EdgeInsets.all(1.0),
        ),
      ),
    );
  }

  List<StaggeredTile> _staggeredTiles = const <StaggeredTile>[
    const StaggeredTile.fit(1),
    const StaggeredTile.fit(1),
    const StaggeredTile.fit(1),
    const StaggeredTile.fit(1),
    const StaggeredTile.fit(1),
    const StaggeredTile.fit(1),
    const StaggeredTile.fit(1),
    const StaggeredTile.fit(1),
    const StaggeredTile.fit(2),
  ];

  List<Widget> _tiles = const <Widget>[
    HomeGridTiles(icon: Icons.drafts, title: StringsMyApp.member_bill_payment),
    HomeGridTiles(icon: Icons.dashboard, title: StringsMyApp.online_reservation),
    HomeGridTiles(icon: Icons.wb_sunny, title: StringsMyApp.home_delivery_order),
    HomeGridTiles(icon: Icons.directions_car, title: StringsMyApp.transaction_history),
    HomeGridTiles(icon: Icons.account_box, title: StringsMyApp.view_profile),
    HomeGridTiles(icon: Icons.calendar_today, title: StringsMyApp.events_calendar),
    HomeGridTiles(icon: Icons.notifications_active, title: StringsMyApp.important_notices),
    HomeGridTiles(icon: Icons.print, title: StringsMyApp.member_directory),
    HomeGridTiles(icon: Icons.feedback, title: StringsMyApp.post_your_feedback),
  ];
}
