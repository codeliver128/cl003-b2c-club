import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/profile/ViewProfileScreen.dart';
import 'package:delhi_gymkhana_club/utils/NavigationRouteUtil.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';
import 'HomeScreenGrid.dart';
import 'HomeScreenHeader.dart';
import 'package:delhi_gymkhana_club/navigationdrawer/DrawerConstants.dart';
import 'package:delhi_gymkhana_club/forgotpassword/ForgotPasswordScreen.dart';
import 'package:delhi_gymkhana_club/constants/ImageAssetsConstants.dart';
import 'package:delhi_gymkhana_club/membershipenquiry/MemberShipEnquiryScreen.dart';

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() => new HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  int _selectedDrawerIndex = 0;

  _onSelectItem(int index) {
    setState(() {
      _selectedDrawerIndex = index;
    });
    var d = DrawerConstants.drawerItems[_selectedDrawerIndex];
    Navigator.of(context).pop(); // close the drawer
    d.title == StringsMyApp.log_out
        ? _logout()
        : NavigationRouteUtil().navigateToScreen(context, d.title);
  }

  Widget buildBody(BuildContext context, int index) {
    var d = DrawerConstants.drawerItems[index];
    if (index == 0) {
      return Column(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(left: 16, right: 16, top: 16),
              child: Image.asset(ImageAssetsConstants.app_logo)),
          new Divider(),
          new InkWell(
            onTap: () {
              _onSelectItem(index);
            },
            child: Row(children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 20),
              ),
              Icon(
                d.icon,
                color: ColorsMyApp.greyDark,
              ),
              Padding(
                  padding: EdgeInsets.only(top: 10, bottom: 10, left: 20),
                  child: Text(
                    d.title,
                    style: TextStyle(
                        color: ColorsMyApp.black_low,
                        fontWeight: FontWeight.w500),
                  )),
            ]),
          ),
          new Divider()
        ],
      );
    } else {
      return Column(
        children: <Widget>[
          new InkWell(
            onTap: () {
              _onSelectItem(index);
            },
            child: Row(children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 20),
              ),
              Icon(
                d.icon,
                color: ColorsMyApp.greyDark,
              ),
              Padding(
                  padding: EdgeInsets.only(top: 10, bottom: 10, left: 20),
                  child: Text(
                    d.title,
                    style: TextStyle(
                        color: ColorsMyApp.black_low,
                        fontWeight: FontWeight.w500),
                  )),
            ]),
          ),
          new Divider()
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.home,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      drawer: Drawer(
        child: new ListView.builder(
          itemCount: DrawerConstants.drawerItems.length,
          itemBuilder: (BuildContext ctxt, int index) => buildBody(ctxt, index),
        ),
      ),
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          child: Column(
            children: <Widget>[
              HomeScreenHeader(),
              HomeScreenGrid(),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _logout() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: new Text(StringsMyApp.log_out),
          content: new Text(StringsMyApp.logout_msg),
          actions: <Widget>[
            new FlatButton(
              child: new Text(StringsMyApp.confirm),
              onPressed: () {
                Toast.show("Log Out Successful.", context,gravity: Toast.BOTTOM,duration: Toast.LENGTH_LONG);
                Navigator.of(context).pop();
                NavigationRouteUtil().navigateToScreen(
                    context, StringsMyApp.login,
                    removePreviousScreens: true);
              },
            ),
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(StringsMyApp.cancel),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
