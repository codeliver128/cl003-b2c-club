class MemberDirectoryModel {
  String _memberId;
  String _memberName;
  String _memberMobileNo;

  MemberDirectoryModel({memberId, memberName, memberMobileNo})
      : _memberId = memberId,
        _memberName = memberName,
        _memberMobileNo = memberMobileNo;

  String get memberMobileNo => _memberMobileNo;

  set memberMobileNo(String value) {
    _memberMobileNo = value;
  }

  String get memberName => _memberName;

  set memberName(String value) {
    _memberName = value;
  }

  String get memberId => _memberId;

  set memberId(String value) {
    _memberId = value;
  }

  getMemberDirectoryList() {
    return [
      MemberDirectoryModel(
          memberId: "P-5254",
          memberName: "Laxman Singh",
          memberMobileNo: "8976765454"),
      MemberDirectoryModel(
          memberId: "P-5254",
          memberName: "Laxman Singh",
          memberMobileNo: "8976765454"),
      MemberDirectoryModel(
          memberId: "P-5254",
          memberName: "Laxman Singh",
          memberMobileNo: "8976765454"),
      MemberDirectoryModel(
          memberId: "P-5254",
          memberName: "Laxman Singh",
          memberMobileNo: "8976765454"),
      MemberDirectoryModel(
          memberId: "P-5254",
          memberName: "Laxman Singh",
          memberMobileNo: "8976765454"),
      MemberDirectoryModel(
          memberId: "P-5254",
          memberName: "Laxman Singh",
          memberMobileNo: "8976765454"),
      MemberDirectoryModel(
          memberId: "P-5254",
          memberName: "Laxman Singh",
          memberMobileNo: "8976765454"),
      MemberDirectoryModel(
          memberId: "P-5254",
          memberName: "Laxman Singh",
          memberMobileNo: "8976765454"),
      MemberDirectoryModel(
          memberId: "P-5254",
          memberName: "Laxman Singh",
          memberMobileNo: "8976765454"),
      MemberDirectoryModel(
          memberId: "P-5254",
          memberName: "Laxman Singh",
          memberMobileNo: "8976765454"),
      MemberDirectoryModel(
          memberId: "P-5254",
          memberName: "Laxman Singh",
          memberMobileNo: "8976765454"),
      MemberDirectoryModel(
          memberId: "P-5254",
          memberName: "Laxman Singh",
          memberMobileNo: "8976765454"),
      MemberDirectoryModel(
          memberId: "P-5254",
          memberName: "Laxman Singh",
          memberMobileNo: "8976765454"),
      MemberDirectoryModel(
          memberId: "P-5254",
          memberName: "Laxman Singh",
          memberMobileNo: "8976765454"),
    ];
  }
}
