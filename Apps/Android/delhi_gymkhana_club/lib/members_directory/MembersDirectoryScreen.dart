import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils/NavigationRouteUtil.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';

import 'models/MemberDirectoryModel.dart';

class MembersDirectoryScreen extends StatefulWidget {
  @override
  _MembersDirectoryScreenState createState() => _MembersDirectoryScreenState();
}

class _MembersDirectoryScreenState extends State<MembersDirectoryScreen> {
  bool showContainer = false;

  List<MemberDirectoryModel> listMemberDirectory =
      MemberDirectoryModel().getMemberDirectoryList();

  String _selectedCriteria;
  String valueEntered="";

  List<String> listCriteria = [
    StringsMyApp.mobile_no,
    StringsMyApp.member_id,
    StringsMyApp.name
  ];
  List<DropdownMenuItem<String>> _dropdownMenuItems;

  @override
  void initState() {
    _dropdownMenuItems = buildDropdownMenuItems(listCriteria);
    super.initState();
  }

  List<DropdownMenuItem<String>> buildDropdownMenuItems(List criteria) {
    List<DropdownMenuItem<String>> items = List();
    for (String _criteria in criteria) {
      items.add(
        DropdownMenuItem(
          value: _criteria,
          child: Text(_criteria),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(String selectedCriteria) {
    setState(() {
      _selectedCriteria = selectedCriteria;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.member_directory,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(right: 10, left: 10),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 8),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          StringsMyApp.select_criteria,
                          style: TextStyle(
                              color: ColorsMyApp.themeColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: 0, right: 0, top: 12, bottom: 12),
                        padding: EdgeInsets.only(left: 10, right: 10),
                        width: double.infinity,
                        height: 48,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: ColorsMyApp.grey_border,
                            width: 1.5,
                          ),
                        ),
                        child: DropdownButton(
                            underline: Container(),
                            isExpanded: true,
                            icon: Icon(
                              Icons.keyboard_arrow_down,
                              color: ColorsMyApp.greyDark,
                              size: 32,
                            ),
                            hint: Text(
                              StringsMyApp.select_criteria,
                              style: TextStyle(
                                  color: ColorsMyApp.grey_54, fontSize: 16),
                            ),
                            value: _selectedCriteria,
                            items: _dropdownMenuItems,
                            onChanged: onChangeDropdownItem),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 2),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                StringsMyApp.enter_value,
                style: TextStyle(
                    color: ColorsMyApp.themeColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.editBoxBorderColor,
                  width: 1,
                ),
                // borderRadius: BorderRadius.circular(20.0),
              ),
              margin: const EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                children: <Widget>[
                  new Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 6.0),
                  ),
                  new Expanded(
                    child: TextField(
                      obscureText: false,
                      controller: TextEditingController(text: valueEntered),
                      onChanged: (text) {
                        valueEntered = text;
                      },
                      cursorColor: ColorsMyApp.VividOrange,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                          labelStyle: TextStyle(color: Colors.white),
                          border: InputBorder.none,
                          hintText: StringsMyApp.enter_value),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 8),
            ),
            Row(
              children: <Widget>[
                Expanded(
                    flex: 1,
                    child: Button(
                      buttonText: StringsMyApp.search,
                      onButtonClickFunction: _clickButtonSearch,
                      buttonTextColor: Colors.white,
                    )),
                Padding(
                  padding: EdgeInsets.only(left: 10),
                ),
                Expanded(
                  flex: 1,
                  child: Button(
                    buttonText: StringsMyApp.reset,
                    buttonBackGroundColor: ColorsMyApp.black_low,
                    onButtonClickFunction: _clickButtonReset,
                    buttonTextColor: Colors.white,
                  ),
                )
              ],
            ),
            Expanded(
              child: Visibility(
                visible: showContainer,
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  color: Colors.black12,
                  child: Column(children: <Widget>[
                    ListTile(
                      title: Row(
                        children: <Widget>[
                          Expanded(
                            child: Align(
                              alignment: Alignment.center,
                              child: Text(
                                StringsMyApp.member_id,
                                style: TextStyle(
                                    fontSize: 13, fontWeight: FontWeight.bold),
                              ),
                            ),
                            flex: 1,
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.center,
                              child: Text(
                                StringsMyApp.name,
                                style: TextStyle(
                                    fontSize: 13, fontWeight: FontWeight.bold),
                              ),
                            ),
                            flex: 2,
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.center,
                              child: Text(
                                StringsMyApp.mobile_no,
                                style: TextStyle(
                                    fontSize: 13, fontWeight: FontWeight.bold),
                              ),
                            ),
                            flex: 1,
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: listMemberDirectory.length,
                        itemBuilder: (context, position) {
                          return _getListRow(context, position);
                        },
                      ),
                    ),
                  ]),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _clickButtonSearch(BuildContext context) {
    setState(() {
      showContainer = true;
    });
  }

  _clickButtonReset(BuildContext context) {
    setState(() {
      showContainer = false;
      _selectedCriteria = null;
      valueEntered = "";
    });
    print("Valueeeee $valueEntered");
  }

  _launchCaller(String number) async {
    String url = "tel:$number";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _getListRow(BuildContext context, int position) {
    return Card(
      elevation: 0,
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.only(top: 6, bottom: 6),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  listMemberDirectory[position].memberId,
                  style: TextStyle(fontSize: 12),
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  listMemberDirectory[position].memberName,
                  style: TextStyle(fontSize: 12),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.center,
                child: InkWell(
                  onTap: () {
                    _launchCaller(listMemberDirectory[position].memberMobileNo);
                  },
                  child: Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    child: Text(
                      listMemberDirectory[position].memberMobileNo,
                      style: TextStyle(fontSize: 12),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
