import 'package:delhi_gymkhana_club/navigationdrawer/DrawerItem.dart';
import 'package:flutter/material.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';

class DrawerConstants {
  static final drawerItems = [
    new DrawerItem(title: StringsMyApp.change_password, icon: Icons.track_changes),
    new DrawerItem(title: StringsMyApp.member_bill_payment, icon: Icons.drafts),
    new DrawerItem(title: StringsMyApp.online_reservation, icon: Icons.dashboard),
    new DrawerItem(title: StringsMyApp.home_delivery_order, icon: Icons.wb_sunny),
    new DrawerItem(title: StringsMyApp.transaction_history, icon: Icons.directions_car),
    new DrawerItem(title: StringsMyApp.view_profile, icon: Icons.account_box),
    new DrawerItem(title: StringsMyApp.events_calendar, icon: Icons.calendar_today),
    new DrawerItem(title: StringsMyApp.important_notices, icon: Icons.notifications_active),
    new DrawerItem(title: StringsMyApp.member_directory, icon: Icons.print),
    new DrawerItem(title: StringsMyApp.post_your_feedback, icon: Icons.feedback),
    new DrawerItem(title: StringsMyApp.log_out, icon: Icons.call_missed_outgoing),
  ];
}
