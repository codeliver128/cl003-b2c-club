class SportsTypeModel {
  String _id;
  String _title;

  SportsTypeModel({id, title})
      : _id = id,
        _title = title;




  String get id => _id;

  set id(String value) {
    _id = value;
  }

  String get title => _title;

  set title(String value) {
    _title = value;
  }


  getSportsType(){
    return [
      SportsTypeModel(id: "1", title: "Cricket"),
      SportsTypeModel(id: "1", title: "Baseball"),
      SportsTypeModel(id: "1", title: "Football"),
      SportsTypeModel(id: "1", title: "Badminton"),
      SportsTypeModel(id: "1", title: "Kabbadi"),
      SportsTypeModel(id: "1", title: "Billiards"),
    ];
  }
}
