class SlotsDataModel {
  String _id;
  String _startTime;
  String _endTime;
  bool _isSlotAvailable;
  bool _isSelected=false;

  SlotsDataModel({id, startTime, endTime, isSlotAvailable})
      : _startTime = startTime,
        _endTime = endTime,
        _id = id,
        _isSlotAvailable = isSlotAvailable;

  bool get isSlotAvailable => _isSlotAvailable;

  set isSlotAvailable(bool value) {
    _isSlotAvailable = value;
  }

  String get endTime => _endTime;

  set endTime(String value) {
    _endTime = value;
  }

  String get startTime => _startTime;

  set startTime(String value) {
    _startTime = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  bool get isSelected => _isSelected;

  set isSelected(bool value) {
    _isSelected = value;
  }



  getSlotsList(){
    return [
      SlotsDataModel(
          id: "1",
          startTime: "05:00 AM",
          endTime: "06:00 AM",
          isSlotAvailable: true),
      SlotsDataModel(
          id: "2",
          startTime: "06:00 AM",
          endTime: "07:00 AM",
          isSlotAvailable: false),
      SlotsDataModel(
          id: "3",
          startTime: "07:00 AM",
          endTime: "08:00 AM",
          isSlotAvailable: false),
      SlotsDataModel(
          id: "4",
          startTime: "08:00 AM",
          endTime: "09:00 AM",
          isSlotAvailable: true),
      SlotsDataModel(
          id: "5",
          startTime: "09:00 AM",
          endTime: "10:00 AM",
          isSlotAvailable: false),
      SlotsDataModel(
          id: "6",
          startTime: "10:00 AM",
          endTime: "11:00 AM",
          isSlotAvailable: false),
      SlotsDataModel(
          id: "7",
          startTime: "11:00 AM",
          endTime: "12:00 PM",
          isSlotAvailable: true),
      SlotsDataModel(
          id: "8",
          startTime: "12:00 PM",
          endTime: "01:00 PM",
          isSlotAvailable: true),
      SlotsDataModel(
          id: "9",
          startTime: "02:00 PM",
          endTime: "03:00 PM",
          isSlotAvailable: false),
      SlotsDataModel(
          id: "10",
          startTime: "03:00 PM",
          endTime: "04:00 PM",
          isSlotAvailable: false),
      SlotsDataModel(
          id: "11",
          startTime: "04:00 PM",
          endTime: "05:00 PM",
          isSlotAvailable: true),
      SlotsDataModel(
          id: "12",
          startTime: "05:00 PM",
          endTime: "06:00 PM",
          isSlotAvailable: false),
      SlotsDataModel(
          id: "13",
          startTime: "06:00 PM",
          endTime: "07:00 PM",
          isSlotAvailable: true),
      SlotsDataModel(
          id: "14",
          startTime: "07:00 PM",
          endTime: "08:00 PM",
          isSlotAvailable: false),
      SlotsDataModel(
          id: "15",
          startTime: "08:00 PM",
          endTime: "09:00 PM",
          isSlotAvailable: true),
    ];
  }


}
