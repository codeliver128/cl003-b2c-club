import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/Constants.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/online_reservations/sports_slot_reservation/SlotsDataModel.dart';
import 'package:delhi_gymkhana_club/online_reservations/sports_slot_reservation/SportsTypeModel.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:toast/toast.dart';

class SportSlotReservationScreen extends StatefulWidget {

  @override
  _SportSlotReservationScreenState createState() =>
      _SportSlotReservationScreenState();
}

class _SportSlotReservationScreenState
    extends State<SportSlotReservationScreen> {
  DateTime selected;

  bool isSlotSelected = false;

  String dropdownValue;
  int currentSelectedIndex = -1;
  List<SlotsDataModel> listSlotModel = SlotsDataModel().getSlotsList();

  SportsTypeModel _selectedSport;
  List<SportsTypeModel> listSports = SportsTypeModel().getSportsType();
  List<DropdownMenuItem<SportsTypeModel>> _dropdownMenuItems;

  @override
  void initState() {
    _dropdownMenuItems = buildDropdownMenuItems(listSports);
    super.initState();
  }

  List<DropdownMenuItem<SportsTypeModel>> buildDropdownMenuItems(List sports) {
    List<DropdownMenuItem<SportsTypeModel>> items = List();
    for (SportsTypeModel sport in sports) {
      items.add(
        DropdownMenuItem(
          value: sport,
          child: Text(sport.title),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(SportsTypeModel selectedSport) {
    setState(() {
      _selectedSport = selectedSport;
    });
  }

  _selectDate() async {
    selected = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(2019),
      lastDate: new DateTime(2020),
    );
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.sports_Slot_reservation,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(left: 16, right: 16, bottom: 10, top: 10),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              width: double.infinity,
              height: 48,
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.grey_border,
                  width: 1.5,
                ),
              ),
              child: DropdownButton(
                  underline: Container(),
                  isExpanded: true,
                  icon: Icon(
                    Icons.keyboard_arrow_down,
                    color: ColorsMyApp.greyDark,
                    size: 32,
                  ),
                  hint: Text(
                    StringsMyApp.select_sports,
                    style: TextStyle(color: ColorsMyApp.grey_54, fontSize: 16),
                  ),
                  value: _selectedSport,
                  items: _dropdownMenuItems,
                  onChanged: onChangeDropdownItem),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Container(
              width: double.infinity,
              height: 50,
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.grey_border,
                  width: 1.5,
                ),
              ),
              child: InkWell(
                onTap: () {
                  _selectDate();
                },
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 4,
                        child: Text(
                          selected != null
                              ? new DateFormat('yyyy-MM-dd').format(selected)
                              : StringsMyApp.select_date,
                          style: TextStyle(
                              fontSize: 16,
                              color: selected != null
                                  ? Colors.black
                                  : ColorsMyApp.grey_54),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Icon(
                            Icons.calendar_today,
                            color: ColorsMyApp.greyDark,
                            size: 22,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Expanded(
              flex: 6,
              child: GridView.count(
                shrinkWrap: true,
                childAspectRatio: 3 / 1,
                crossAxisSpacing: 3,
                mainAxisSpacing: 3,
                // Create a grid with 2 columns. If you change the scrollDirection to
                // horizontal, this produces 2 rows.
                crossAxisCount: 2,
                // Generate 100 widgets that display their index in the List.
                children: List.generate(
                  listSlotModel.length,
                  (index) {
                    return Container(
                      color: _getBackgroundColor(listSlotModel[index]),
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            if (listSlotModel[index].isSlotAvailable) {
                              if (currentSelectedIndex >= 0) {
                                listSlotModel[currentSelectedIndex].isSelected = false;
                                currentSelectedIndex = index;
                                listSlotModel[index].isSelected = true;
                                isSlotSelected=true;
                              } else {
                                currentSelectedIndex = index;
                                listSlotModel[index].isSelected = true;
                                isSlotSelected=true;
                              }
                            }
                          });
                        },
                        child: Center(
                          child: Text(
                            listSlotModel[index].startTime +
                                " - " +
                                listSlotModel[index].endTime,
                            style: TextStyle(fontSize: 12, color: Colors.black),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Row(
                children: <Widget>[
                  Container(
                    width: 18,
                    height: 18,
                    margin: EdgeInsets.only(right: 10),
                    color: ColorsMyApp.available_color,
                    padding: EdgeInsets.only(
                        top: 10, bottom: 10, left: 16, right: 16),
                  ),
                  Text(StringsMyApp.available),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                  ),
                  Container(
                    width: 18,
                    height: 18,
                    margin: EdgeInsets.only(right: 10),
                    color: ColorsMyApp.unavailable_color,
                    padding: EdgeInsets.only(
                        top: 10, bottom: 10, left: 16, right: 16),
                  ),
                  Text(StringsMyApp.unavailable),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: Button(
                    buttonText: StringsMyApp.reserve,
                    onButtonClickFunction: _clickButton,
                    buttonTextColor: Colors.white,
                    buttonBackGroundColor: currentSelectedIndex < 0
                        ? ColorsMyApp.button_disabled
                        : ColorsMyApp.button_enabled,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _getBackgroundColor(SlotsDataModel slotsDataModel) {
    if (slotsDataModel.isSelected) {
      return ColorsMyApp.selected_slot_box_color;
    } else {
      if (slotsDataModel.isSlotAvailable) {
        return ColorsMyApp.available_color;
      } else {
        return ColorsMyApp.unavailable_color;
      }
    }
  }

  _clickButton(BuildContext context) {

    if (_selectedSport == null) {
      Toast.show("Please select sport.", context,
          gravity: Toast.BOTTOM, duration: Toast.LENGTH_SHORT);
      return;
    }

    if (selected == null) {
      Toast.show("Please select date.", context,
          gravity: Toast.BOTTOM, duration: Toast.LENGTH_SHORT);
      return;
    }



    if (isSlotSelected == false) {
      Toast.show("Please Select slot to reserve.", context,
          gravity: Toast.BOTTOM, duration: Toast.LENGTH_SHORT);
      return;
    }
    Toast.show("Slot Reserved Successfully.", context,
        gravity: Toast.BOTTOM, duration: Toast.LENGTH_LONG);

    Navigator.popUntil(context, ModalRoute.withName(HOME_SCREEN));
  }
}
