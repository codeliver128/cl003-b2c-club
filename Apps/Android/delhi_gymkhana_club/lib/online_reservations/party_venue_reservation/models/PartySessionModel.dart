class PartySessionModel {
  String _id;
  String _partySessionTitle;
  String _partySessionTime;

  PartySessionModel({id, partySessionTitle, partySessionTime})
      : _id = id,
        _partySessionTitle = partySessionTitle,
        _partySessionTime = partySessionTime;

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  String get partySessionTitle => _partySessionTitle;

  set partySessionTitle(String value) {
    _partySessionTitle = value;
  }

  String get partySessionTime => _partySessionTime;

  set partySessionTime(String value) {
    _partySessionTime = value;
  }

  getPartySessionList() {
    return [
      PartySessionModel(
          id: "1",
          partySessionTitle: "Lunch",
          partySessionTime: "12:01 PM - 03:00 PM"),
      PartySessionModel(
          id: "1",
          partySessionTitle: "Hi Tea",
          partySessionTime: "03:01 PM - 06:00 PM"),
      PartySessionModel(
          id: "1",
          partySessionTitle: "Dinner",
          partySessionTime: "07:01 PM - 11:00 PM")
    ];
  }
}
