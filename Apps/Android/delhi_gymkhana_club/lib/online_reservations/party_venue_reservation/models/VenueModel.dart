class VenueModel {
  String _id;
  String _venueTitle;

  VenueModel({id, venueTitle})
      : _id = id,
        _venueTitle = venueTitle;

  String get venueTitle => _venueTitle;

  set venueTitle(String value) {
    _venueTitle = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  getVenueList() {
    return [
      VenueModel(id: "1", venueTitle: "Centenary"),
      VenueModel(id: "2", venueTitle: "Blue Room"),
      VenueModel(id: "3", venueTitle: "Jamun Tree"),
      VenueModel(id: "4", venueTitle: "Woodland"),
      VenueModel(id: "5", venueTitle: "Party Cottage"),
    ];
  }
}
