import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/online_reservations/room_reservations/models/BookingForModel.dart';
import 'package:delhi_gymkhana_club/utils/NavigationRouteUtil.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

import 'models/VenueModel.dart';

class AddVenueScreen extends StatefulWidget {
  @override
  _AddVenueScreenState createState() => _AddVenueScreenState();
}

class _AddVenueScreenState extends State<AddVenueScreen> {
  String tarrifPerNight = "3500.00";
  String roomAvailable = "25";
  String noOfRoomsToBeBooked;

  String functionDate = "21 Aug 2019", session = "Hi Tea  03:01 PM - 06:00 PM";

  VenueModel _selectedVenue;
  List<VenueModel> listVenue = VenueModel().getVenueList();
  List<DropdownMenuItem<VenueModel>> _dropdownMenuItemsVenue;

  List<DropdownMenuItem<VenueModel>> buildDropdownMenuItemsVenue(
      List venueModel) {
    List<DropdownMenuItem<VenueModel>> items = List();
    for (VenueModel venuemodel in venueModel) {
      items.add(
        DropdownMenuItem(
          value: venuemodel,
          child: Text(venuemodel.venueTitle),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItemVenue(VenueModel selectedVenue) {
    setState(() {
      _selectedVenue = selectedVenue;
    });
  }

  BookingForModel _selectedBookingFor;
  List<BookingForModel> listBookingFor = BookingForModel().getBookingForList();
  List<DropdownMenuItem<BookingForModel>> _dropdownMenuItemsBookingFor;

  @override
  void initState() {
    _dropdownMenuItemsBookingFor = buildDropdownMenuItems(listBookingFor);
    _dropdownMenuItemsVenue = buildDropdownMenuItemsVenue(listVenue);
    super.initState();
  }

  List<DropdownMenuItem<BookingForModel>> buildDropdownMenuItems(
      List bookingformodel) {
    List<DropdownMenuItem<BookingForModel>> items = List();
    for (BookingForModel bookingfor in bookingformodel) {
      items.add(
        DropdownMenuItem(
          value: bookingfor,
          child: Text(bookingfor.bookingForTitle),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(BookingForModel selectedBookingFor) {
    setState(() {
      _selectedBookingFor = selectedBookingFor;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.party_venue_booking,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 4, right: 10, left: 10, bottom: 10),
        child: Column(
          children: <Widget>[
            Card(
              elevation: 5,
              color: Colors.white,
              child: Container(
                padding: EdgeInsets.only(top: 12, bottom: 12),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Column(
                        children: <Widget>[
                          Text(
                            StringsMyApp.function_date,
                            style: TextStyle(
                                color: ColorsMyApp.themeColor,
                                fontWeight: FontWeight.bold),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 12),
                          ),
                          Text(
                            functionDate,
                            style: TextStyle(
                                color: ColorsMyApp.black_low, fontSize: 12),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Column(
                        children: <Widget>[
                          Text(
                            StringsMyApp.session,
                            style: TextStyle(
                                color: ColorsMyApp.themeColor,
                                fontWeight: FontWeight.bold),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 12),
                          ),
                          Text(
                            session,
                            style: TextStyle(
                                color: ColorsMyApp.black_low, fontSize: 12),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                StringsMyApp.booking_for,
                style: TextStyle(
                    color: ColorsMyApp.themeColor, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 0, right: 0, top: 12, bottom: 12),
              padding: EdgeInsets.only(left: 10, right: 10),
              width: double.infinity,
              height: 48,
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.grey_border,
                  width: 1.5,
                ),
              ),
              child: DropdownButton(
                  underline: Container(),
                  isExpanded: true,
                  icon: Icon(
                    Icons.keyboard_arrow_down,
                    color: ColorsMyApp.greyDark,
                    size: 32,
                  ),
                  hint: Text(
                    StringsMyApp.select,
                    style: TextStyle(color: ColorsMyApp.grey_54, fontSize: 16),
                  ),
                  value: _selectedBookingFor,
                  items: _dropdownMenuItemsBookingFor,
                  onChanged: onChangeDropdownItem),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10, left: 8),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                StringsMyApp.select_venue,
                style: TextStyle(
                    color: ColorsMyApp.themeColor, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 0, right: 0, top: 12, bottom: 12),
              padding: EdgeInsets.only(left: 10, right: 10),
              width: double.infinity,
              height: 48,
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.grey_border,
                  width: 1.5,
                ),
              ),
              child: DropdownButton(
                  underline: Container(),
                  isExpanded: true,
                  icon: Icon(
                    Icons.keyboard_arrow_down,
                    color: ColorsMyApp.greyDark,
                    size: 32,
                  ),
                  hint: Text(
                    StringsMyApp.select_venue,
                    style: TextStyle(color: ColorsMyApp.grey_54, fontSize: 16),
                  ),
                  value: _selectedVenue,
                  items: _dropdownMenuItemsVenue,
                  onChanged: onChangeDropdownItemVenue),
            ),
            Padding(
              padding: EdgeInsets.only(top: 16),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                StringsMyApp.remarks,
                style: TextStyle(
                    color: ColorsMyApp.themeColor, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              height: 100,
              padding: EdgeInsets.only(left: 10, right: 10),
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.editBoxBorderColor,
                  width: 1,
                ),
                // borderRadius: BorderRadius.circular(20.0),
              ),
              margin: const EdgeInsets.symmetric(vertical: 10.0),
              child: TextField(
                onChanged: (text) {
                  setState(() {
                    noOfRoomsToBeBooked = text;
                  });
                },
                cursorColor: ColorsMyApp.VividOrange,
                keyboardType: TextInputType.text,
                maxLines: 5,
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    labelStyle: TextStyle(color: Colors.black),
                    border: InputBorder.none,
                    hintText: StringsMyApp.remarks_if_any),
              ),
            ),
            _getRows("Venue Charge", "45000.00"),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: Button(
                    buttonText: StringsMyApp.next,
                    onButtonClickFunction: _clickButton,
                    buttonTextColor: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _clickButton(BuildContext context) {
    NavigationRouteUtil()
        .navigateToScreen(context, StringsMyApp.party_venue_billing_screen);
  }


  _getRows(String title, String value) {
    return Container(
        margin: EdgeInsets.only(top: 4),
        color: ColorsMyApp.grey_21,
        padding: EdgeInsets.only(top: 15, bottom: 15, left: 12, right: 12),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text(
                title,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: ColorsMyApp.greyDark,
                    fontSize: 13),
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(
                value,
                textAlign: TextAlign.right,
                style: TextStyle(color: ColorsMyApp.greyDark, fontSize: 13),
              ),
            )
          ],
        ));
  }
}
