import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/DateTimeFormatConstants.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils/NavigationRouteUtil.dart';
import 'package:delhi_gymkhana_club/utils_widget/BulletWidget.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:toast/toast.dart';

import 'models/PartySessionModel.dart';

class PartyVenueAvailabllityScreen extends StatefulWidget {
  @override
  _PartyVenueAvailabllityScreenState createState() =>
      _PartyVenueAvailabllityScreenState();
}

class _PartyVenueAvailabllityScreenState
    extends State<PartyVenueAvailabllityScreen> {
  DateTime selectedFunctionDate;

  PartySessionModel _selectedPartySession;
  List<PartySessionModel> listPartySession = PartySessionModel().getPartySessionList();
  List<DropdownMenuItem<PartySessionModel>> _dropdownMenuItems;

  @override
  void initState() {
    _dropdownMenuItems = buildDropdownMenuItems(listPartySession);
    super.initState();
  }

  List<DropdownMenuItem<PartySessionModel>> buildDropdownMenuItems(
      List partysessions) {
    List<DropdownMenuItem<PartySessionModel>> items = List();
    for (PartySessionModel session in partysessions) {
      items.add(
        DropdownMenuItem(
          value: session,
          child: Text(session.partySessionTitle+"     "+session.partySessionTime,style:TextStyle(fontSize: 14),),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(PartySessionModel selectedPartySession) {
    setState(() {
      _selectedPartySession = selectedPartySession;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.party_venue_booking,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        margin: EdgeInsets.only(top: 16, left: 16, bottom: 16, right: 16),
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              StringsMyApp.function_date,
              style: TextStyle(
                  color: ColorsMyApp.themeColor,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Container(
              width: double.infinity,
              height: 50,
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.grey_border,
                  width: 1.5,
                ),
              ),
              child: InkWell(
                onTap: () {
                  _selectDateCheckIn();
                },
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 4,
                        child: Text(
                          selectedFunctionDate != null
                              ? new DateFormat(
                                      DateTimeFormatConstants.yyyy_MM_dd)
                                  .format(selectedFunctionDate)
                              : StringsMyApp.select_date,
                          style: TextStyle(
                              fontSize: 16,
                              color: selectedFunctionDate != null
                                  ? Colors.black
                                  : ColorsMyApp.grey_54),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Icon(
                            Icons.calendar_today,
                            color: ColorsMyApp.greyDark,
                            size: 22,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              padding: EdgeInsets.only(left: 10, right: 5,top: 2,bottom: 2),
              width: double.infinity,
              height: 50,
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.grey_border,
                  width: 1.5,
                ),
              ),
              child: DropdownButton(
                  underline: Container(),
                  isExpanded: true,
                  icon: Icon(
                    Icons.keyboard_arrow_down,
                    color: ColorsMyApp.greyDark,
                    size: 32,
                  ),
                  hint: Text(
                    StringsMyApp.select_session,
                    style: TextStyle(color: ColorsMyApp.grey_54, fontSize: 16),
                  ),
                  value: _selectedPartySession,
                  items: _dropdownMenuItems,
                  onChanged: onChangeDropdownItem),
            ),
            Padding(
              padding: EdgeInsets.only(top: 26),
            ),
            Padding(
              padding: EdgeInsets.only(top: 32),
            ),
            Button(
              buttonText: StringsMyApp.check_availability,
              buttonBackGroundColor: ColorsMyApp.themeColor,
              onButtonClickFunction: _clickButton,
              buttonTextColor: Colors.white,
            ),
            Padding(
              padding: EdgeInsets.only(top: 36),
            ),
            NoteSection(),
          ],
        ),
      ),
    );
  }

  _selectDateCheckIn() async {
    var date = DateTime.now();
    selectedFunctionDate = await showDatePicker(
      context: context,
      initialDate: date,
      firstDate: new DateTime(date.year, date.month, date.day),
      lastDate: new DateTime(2020),
    );
    setState(() {});
  }

  _clickButton(BuildContext context) {
    if (selectedFunctionDate == null) {
      Toast.show(StringsMyApp.select_check_in_date, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      return;
    }
    NavigationRouteUtil().navigateToScreen(context, StringsMyApp.add_venue_screen);
  }
}

class NoteSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            StringsMyApp.note_colon,
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          Divider(),
          Text(
            StringsMyApp.note_party_venue_header,
            style: TextStyle(color: Colors.black87),
          ),
          BulletWidget(
            text: StringsMyApp.note_party_venue_note1,
            bulletColor: ColorsMyApp.greyDark,
          ),
          BulletWidget(
            text: StringsMyApp.note_party_venue_note2,
            bulletColor: ColorsMyApp.greyDark,
          ),
        ],
      ),
    );
  }
}
