import 'package:delhi_gymkhana_club/arguments/WebviewArguments.dart';
import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/Constants.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils/RadioListWidget.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';

class PartyVenueBillDetailScreen extends StatefulWidget {
  @override
  _PartyVenueBillDetailScreenState createState() =>
      _PartyVenueBillDetailScreenState();
}

class _PartyVenueBillDetailScreenState
    extends State<PartyVenueBillDetailScreen> {
  String _radioButtonSelected = "";
  bool termsAndConditionAccepted = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.party_venue_booking,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 4, left: 16, right: 16, bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // _getRows(StringsMyApp.member_id, "P-6564"),
            // _getRows(StringsMyApp.member_name, "Laxman Singh"),
            _getRows(StringsMyApp.function_date, "22 Aug 2019"),
            _getRows(StringsMyApp.venue, "Centenary Lawns"),
            _getRows(StringsMyApp.session, "Hi Tea 03:01 PM - 06:00 PM"),
            _getRows(StringsMyApp.relation, "member"),
            _getRows(StringsMyApp.venue_charges, "45000.00"),
            _getRows(StringsMyApp.gst_percent, "18"),
            _getRows(StringsMyApp.gst_amount, "8100.00"),
            Container(
              padding:
                  EdgeInsets.only(top: 16, bottom: 16, left: 10, right: 10),
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.editBoxBorderColor,
                  width: 1,
                ),
                // borderRadius: BorderRadius.circular(20.0),
              ),
              margin: const EdgeInsets.symmetric(vertical: 6.0),
              child: Row(
                children: <Widget>[
                  new Expanded(
                    child: Text(
                      StringsMyApp.total_amount,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  new Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text("53100.00"),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                StringsMyApp.mode_of_payment,
                style: TextStyle(
                    color: ColorsMyApp.themeColor, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 4),
            ),
            Expanded(
              child: RadioListWidget(
                callback: (val) => setState(() => _radioButtonSelected = val),
                listOfRadioValues: StringsMyApp.payment_mode_list,
                selectedRadioButton: _radioButtonSelected,
              ),
            ),
            //getsd(),
            _getTermsAndConditionsRow(),
            Button(
              buttonTextColor: Colors.white,
              buttonText: StringsMyApp.proceed_payment,
              onButtonClickFunction: _clickButton,
            ),
          ],
        ),
      ),
    );
  }

  _clickButton(BuildContext context) {
    if (_radioButtonSelected.isEmpty) {
      Toast.show(StringsMyApp.pls_select_payment_option, context,
          gravity: Toast.BOTTOM);
      return;
    }
    if (!termsAndConditionAccepted) {
      Toast.show(StringsMyApp.accept_terms_conditions, context,
          gravity: Toast.BOTTOM);
      return;
    }
    Toast.show("Party Venue Booked Successfully.", context, gravity: Toast.BOTTOM);
    Navigator.popUntil(context, ModalRoute.withName(HOME_SCREEN));
  }

  _getRows(String title, String value) {
    return Container(
        margin: EdgeInsets.only(top: 2),
        color: ColorsMyApp.grey_21,
        padding: EdgeInsets.only(top: 12, bottom: 12, left: 12, right: 12),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text(
                title,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: ColorsMyApp.greyDark,
                    fontSize: 13),
              ),
            ),
            Expanded(
              flex: 2,
              child: Text(
                value,
                textAlign: TextAlign.right,
                style: TextStyle(color: ColorsMyApp.greyDark, fontSize: 13),
              ),
            )
          ],
        ));
  }

  _getTermsAndConditionsRow() {
    return Container(
      child: Row(
        children: <Widget>[
          Checkbox(
            value: termsAndConditionAccepted,
            activeColor: ColorsMyApp.VividOrange,
            onChanged: (bool value) {
              setState(() {
                termsAndConditionAccepted = value;
              });
            },
          ),
          Expanded(
            child: RichText(
              text: TextSpan(
                text: StringsMyApp.agree_with,
                children: <TextSpan>[
                  TextSpan(
                      text: StringsMyApp.terms_and_conditions,
                      style: TextStyle(
                        color: Colors.blueAccent,
                        decoration: TextDecoration.underline,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Navigator.pushNamed(context, WEBVIEW_SCREEN,
                              arguments: WebviewArguments(
                                  url:
                                      "https://delhigymkhana.org.in/termandcandition.aspx",
                                  title: StringsMyApp.terms_and_conditions));
                        }),
                ],
                style: TextStyle(
                  color: ColorsMyApp.greyDark,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  int _radioValue1 = 1;

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;
    });
  }
}
