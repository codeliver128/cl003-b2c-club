import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/home/HomeScreenHeader.dart';
import 'package:delhi_gymkhana_club/utils/NavigationRouteUtil.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class OnlineReservationsScreen extends StatefulWidget {
  @override
  _OnlineReservationsScreenState createState() => _OnlineReservationsScreenState();
}

class _OnlineReservationsScreenState extends State<OnlineReservationsScreen> {
  List<Map<String, Object>> listOnlineReservations = StringsMyApp.list_online_reservation;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.online_reservation,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 4, bottom: 16),
        child: Column(
          children: <Widget>[
            HomeScreenHeader(),
            Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 10, right: 10),
                  child: ListView.builder(
                    itemCount: listOnlineReservations.length,
                    itemBuilder: (context, position) {
                      return _getListRow(context, position);
                    },
                  ),
                )),
          ],
        ),
      ),
    );
  }

  _getListRow(BuildContext context, int position) {
    return Card(
      elevation: 4,
      color: ColorsMyApp.themeColor,
      child: InkWell(
        onTap: () {
          NavigationRouteUtil().navigateToScreen(
              context, listOnlineReservations.elementAt(position)['title']);
        },
        child: Padding(
          padding: EdgeInsets.only(
            top: 16,
            bottom: 16,
          ),
          child: Center(
            child: Column(
              children: <Widget>[
                Icon(
                  listOnlineReservations.elementAt(position)['icon'],
                  color: Colors.black,
                  size: 30,
                ),
                Text(
                  listOnlineReservations.elementAt(position)['title'],
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

