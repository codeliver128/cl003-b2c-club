class BookingForModel {
  String _id;
  String _bookingForTitle;

  BookingForModel({id, bookingForTitle})
      : _id = id,
        _bookingForTitle = bookingForTitle;

  String get bookingForTitle => _bookingForTitle;

  set bookingForTitle(String value) {
    _bookingForTitle = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  getBookingForList() {
    return [
      BookingForModel(id: "1", bookingForTitle: "Member"),
      BookingForModel(id: "2", bookingForTitle: "Guest"),
    ];
  }
}
