class RoomCategoryModel {
  String _id;
  String _roomCategoryTitle;

  RoomCategoryModel({id, roomCategoryTitle})
      : _id = id,
        _roomCategoryTitle = roomCategoryTitle;

  String get roomCategoryTitle => _roomCategoryTitle;

  set roomCategoryTitle(String value) {
    _roomCategoryTitle = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  getRooomCategoryList() {
    return [
      RoomCategoryModel(id: "1", roomCategoryTitle: "Deluxe"),
      RoomCategoryModel(id: "2", roomCategoryTitle: "Suite"),
      RoomCategoryModel(id: "3", roomCategoryTitle: "Double Suite"),
    ];
  }
}
