import 'package:delhi_gymkhana_club/arguments/WebviewArguments.dart';
import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/Constants.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils/RadioListWidget.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';

class RoomReservationBillingScreen extends StatefulWidget {
  @override
  _RoomReservationBillingScreenState createState() =>
      _RoomReservationBillingScreenState();
}

class _RoomReservationBillingScreenState
    extends State<RoomReservationBillingScreen> {
  String _radioButtonSelected = "";
  bool termsAndConditionAccepted = false;

  String checkInDate = "21 Aug 2019",
      checkOutDate = "22 Aug 2019",
      totalNight = "1";

  List<String> listOfPaymentMode = StringsMyApp.payment_mode_list;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.room_booking,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 0, left: 16, right: 16, bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // _getRows(StringsMyApp.member_id, "P-6564"),
            // _getRows(StringsMyApp.member_name, "Laxman Singh"),
            Card(
              elevation: 5,
              color: Colors.white,
              child: Container(
                padding: EdgeInsets.only(top: 12, bottom: 12),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Text(
                            StringsMyApp.checkin_date,
                            style: TextStyle(
                                color: ColorsMyApp.themeColor,
                                fontWeight: FontWeight.bold),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 12),
                          ),
                          Text(
                            checkInDate,
                            style: TextStyle(
                                color: ColorsMyApp.black_low, fontSize: 12),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Text(
                            StringsMyApp.checkout_date,
                            style: TextStyle(
                                color: ColorsMyApp.themeColor,
                                fontWeight: FontWeight.bold),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 12),
                          ),
                          Text(
                            checkOutDate,
                            style: TextStyle(
                                color: ColorsMyApp.black_low, fontSize: 12),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Text(
                            StringsMyApp.total_nights,
                            style: TextStyle(
                                color: ColorsMyApp.themeColor,
                                fontWeight: FontWeight.bold),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 12),
                          ),
                          Text(totalNight,
                              style: TextStyle(
                                  color: ColorsMyApp.black_low, fontSize: 12)),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 2),
            ),
            _getRows(StringsMyApp.room_category, "Double Suites"),
            _getRows(StringsMyApp.relation, "member"),
            _getRows(StringsMyApp.booked_room, "1"),
            _getRows(StringsMyApp.room_charges, "3500"),
            _getRows(StringsMyApp.gst_percent, "18"),
            _getRows(StringsMyApp.gst_amount, "630.00"),
            Container(
              padding:
                  EdgeInsets.only(top: 16, bottom: 16, left: 10, right: 10),
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.editBoxBorderColor,
                  width: 1,
                ),
                // borderRadius: BorderRadius.circular(20.0),
              ),
              margin: const EdgeInsets.symmetric(vertical: 4.0),
              child: Row(
                children: <Widget>[
                  new Expanded(
                    child: Text(
                      StringsMyApp.total_amount,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  new Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text("4130.00"),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                StringsMyApp.mode_of_payment,
                style: TextStyle(
                    color: ColorsMyApp.themeColor, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 4),
            ),
            Expanded(
              child: RadioListWidget(
                callback: (val) => setState(() => _radioButtonSelected = val),
                listOfRadioValues: StringsMyApp.payment_mode_list,
                selectedRadioButton: _radioButtonSelected,
              ),
            ),
            //getsd(),
            _getTermsAndConditionsRow(),
            Button(
              buttonTextColor: Colors.white,
              buttonText: StringsMyApp.proceed_payment,
              onButtonClickFunction: _clickButton,
            ),
          ],
        ),
      ),
    );
  }

  _clickButton(BuildContext context) {
    if (_radioButtonSelected.isEmpty) {
      Toast.show(StringsMyApp.pls_select_payment_option, context,
          gravity: Toast.BOTTOM);
      return;
    }

    if (!termsAndConditionAccepted) {
      Toast.show(StringsMyApp.accept_terms_conditions, context,
          gravity: Toast.BOTTOM);
      return;
    }
    Toast.show("Room Booked Successfully.", context, gravity: Toast.BOTTOM);
    Navigator.popUntil(context, ModalRoute.withName(HOME_SCREEN));
  }

  _getTermsAndConditionsRow() {
    return Container(
      child: Row(
        children: <Widget>[
          Checkbox(
            value: termsAndConditionAccepted,
            activeColor: ColorsMyApp.VividOrange,
            onChanged: (bool value) {
              setState(() {
                termsAndConditionAccepted = value;
              });
            },
          ),
          Expanded(
            child: RichText(
              text: TextSpan(
                text: StringsMyApp.agree_with,
                children: <TextSpan>[
                  TextSpan(
                      text: StringsMyApp.terms_and_conditions,
                      style: TextStyle(
                        color: Colors.blueAccent,
                        decoration: TextDecoration.underline,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Navigator.pushNamed(context, WEBVIEW_SCREEN,
                              arguments: WebviewArguments(
                                  url:
                                      "https://delhigymkhana.org.in/termandcandition.aspx",
                                  title: StringsMyApp.terms_and_conditions));
                        }),
                ],
                style: TextStyle(
                  color: ColorsMyApp.greyDark,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _getRows(String title, String value) {
    return Container(
        margin: EdgeInsets.only(top: 1.5),
        color: ColorsMyApp.grey_21,
        padding: EdgeInsets.only(top: 12, bottom: 12, left: 12, right: 12),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text(
                title,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: ColorsMyApp.greyDark,
                    fontSize: 13),
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(
                value,
                textAlign: TextAlign.right,
                style: TextStyle(color: ColorsMyApp.greyDark, fontSize: 13),
              ),
            )
          ],
        ));
  }
}
