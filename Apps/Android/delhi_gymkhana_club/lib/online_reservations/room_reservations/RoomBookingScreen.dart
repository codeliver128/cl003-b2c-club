import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/online_reservations/books_reservation/models/BookGenereModel.dart';
import 'package:delhi_gymkhana_club/online_reservations/room_reservations/models/BookingForModel.dart';
import 'package:delhi_gymkhana_club/online_reservations/room_reservations/models/RoomCategoryModel.dart';
import 'package:delhi_gymkhana_club/utils/NavigationRouteUtil.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:delhi_gymkhana_club/utils_widget/InputBoxRectangular.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class RoomBookingScreen extends StatefulWidget {
  @override
  _RoomBookingScreenState createState() => _RoomBookingScreenState();
}

class _RoomBookingScreenState extends State<RoomBookingScreen> {
  String tarrifPerNight="3500.00";
  String roomAvailable="25";
  String noOfRoomsToBeBooked;


  String checkInDate = "21 Aug 2019",
      checkOutDate = "22 Aug 2019",
      totalNight = "1";

  RoomCategoryModel _selectedRoomCategory;
  List<RoomCategoryModel> listRoomCategory =
      RoomCategoryModel().getRooomCategoryList();
  List<DropdownMenuItem<RoomCategoryModel>> _dropdownMenuItemsRoomCategory;

  List<DropdownMenuItem<RoomCategoryModel>> buildDropdownMenuItemsRoomCategory(
      List roomcategorymodel) {
    List<DropdownMenuItem<RoomCategoryModel>> items = List();
    for (RoomCategoryModel roomCategory in roomcategorymodel) {
      items.add(
        DropdownMenuItem(
          value: roomCategory,
          child: Text(roomCategory.roomCategoryTitle),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItemRoomCategory(RoomCategoryModel selectedRoomCategory) {
    setState(() {
      _selectedRoomCategory = selectedRoomCategory;
    });
  }

  BookingForModel _selectedBookingFor;
  List<BookingForModel> listBookingFor = BookingForModel().getBookingForList();
  List<DropdownMenuItem<BookingForModel>> _dropdownMenuItemsBookingFor;

  @override
  void initState() {
    _dropdownMenuItemsBookingFor = buildDropdownMenuItems(listBookingFor);
    _dropdownMenuItemsRoomCategory =
        buildDropdownMenuItemsRoomCategory(listRoomCategory);
    super.initState();
  }

  List<DropdownMenuItem<BookingForModel>> buildDropdownMenuItems(
      List bookingformodel) {
    List<DropdownMenuItem<BookingForModel>> items = List();
    for (BookingForModel bookingfor in bookingformodel) {
      items.add(
        DropdownMenuItem(
          value: bookingfor,
          child: Text(bookingfor.bookingForTitle),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(BookingForModel selectedBookingFor) {
    setState(() {
      _selectedBookingFor = selectedBookingFor;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.room_booking,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 4, right: 10, left: 10, bottom: 10),
        child: Column(
          children: <Widget>[
            Card(
              elevation: 5,
              color: Colors.white,
              child: Container(
                padding: EdgeInsets.only(top: 12, bottom: 12),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Text(
                            StringsMyApp.checkin_date,
                            style: TextStyle(
                                color: ColorsMyApp.themeColor,
                                fontWeight: FontWeight.bold),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 12),
                          ),
                          Text(
                            checkInDate,
                            style: TextStyle(
                                color: ColorsMyApp.black_low, fontSize: 12),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Text(
                            StringsMyApp.checkout_date,
                            style: TextStyle(
                                color: ColorsMyApp.themeColor,
                                fontWeight: FontWeight.bold),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 12),
                          ),
                          Text(
                            checkOutDate,
                            style: TextStyle(
                                color: ColorsMyApp.black_low, fontSize: 12),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Text(
                            StringsMyApp.total_nights,
                            style: TextStyle(
                                color: ColorsMyApp.themeColor,
                                fontWeight: FontWeight.bold),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 12),
                          ),
                          Text(totalNight,
                              style: TextStyle(
                                  color: ColorsMyApp.black_low, fontSize: 12)),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          StringsMyApp.booking_for,
                          style: TextStyle(
                              color: ColorsMyApp.themeColor,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: 0, right: 0, top: 12, bottom: 12),
                        padding: EdgeInsets.only(left: 10, right: 10),
                        width: double.infinity,
                        height: 48,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: ColorsMyApp.grey_border,
                            width: 1.5,
                          ),
                        ),
                        child: DropdownButton(
                            underline: Container(),
                            isExpanded: true,
                            icon: Icon(
                              Icons.keyboard_arrow_down,
                              color: ColorsMyApp.greyDark,
                              size: 32,
                            ),
                            hint: Text(
                              StringsMyApp.select,
                              style: TextStyle(
                                  color: ColorsMyApp.grey_54, fontSize: 16),
                            ),
                            value: _selectedBookingFor,
                            items: _dropdownMenuItemsBookingFor,
                            onChanged: onChangeDropdownItem),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8),
                ),
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Align(
                          alignment: Alignment.centerLeft,
                          child: Text(StringsMyApp.room_category,
                              style: TextStyle(
                                  color: ColorsMyApp.themeColor,
                                  fontWeight: FontWeight.bold))),
                      Container(
                        margin: EdgeInsets.only(
                            left: 0, right: 0, top: 12, bottom: 12),
                        padding: EdgeInsets.only(left: 10, right: 10),
                        width: double.infinity,
                        height: 48,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: ColorsMyApp.grey_border,
                            width: 1.5,
                          ),
                        ),
                        child: DropdownButton(
                            underline: Container(),
                            isExpanded: true,
                            icon: Icon(
                              Icons.keyboard_arrow_down,
                              color: ColorsMyApp.greyDark,
                              size: 32,
                            ),
                            hint: Text(
                              StringsMyApp.select,
                              style: TextStyle(
                                  color: ColorsMyApp.grey_54, fontSize: 16),
                            ),
                            value: _selectedRoomCategory,
                            items: _dropdownMenuItemsRoomCategory,
                            onChanged: onChangeDropdownItemRoomCategory),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 16),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                StringsMyApp.tarrif_per_night,
                style: TextStyle(
                    color: ColorsMyApp.themeColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.editBoxBorderColor,
                  width: 1,
                ),
                // borderRadius: BorderRadius.circular(20.0),
              ),
              margin: const EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                children: <Widget>[
                  new Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 6.0),
                  ),
                  new Expanded(
                    child: TextField(
                      enabled: false,
                      controller: TextEditingController(text: tarrifPerNight ),
                      cursorColor: ColorsMyApp.VividOrange,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(color: Colors.black),
                        border: InputBorder.none,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 16),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                StringsMyApp.rooms_available,
                style: TextStyle(
                    color: ColorsMyApp.themeColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.editBoxBorderColor,
                  width: 1,
                ),
                // borderRadius: BorderRadius.circular(20.0),
              ),
              margin: const EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                children: <Widget>[
                  new Padding(
                    padding:
                    EdgeInsets.symmetric(vertical: 10.0, horizontal: 6.0),
                  ),
                  new Expanded(
                    child: TextField(
                      enabled: false,
                      controller: TextEditingController(text:StringsMyApp.no_of_rooms_avlbl+"($roomAvailable)" ),
                      cursorColor: ColorsMyApp.VividOrange,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(color: Colors.black),
                        border: InputBorder.none,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 16),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                StringsMyApp.rooms_to_be_booked,
                style: TextStyle(
                    color: ColorsMyApp.themeColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.editBoxBorderColor,
                  width: 1,
                ),
                // borderRadius: BorderRadius.circular(20.0),
              ),
              margin: const EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                children: <Widget>[
                  new Padding(
                    padding:
                    EdgeInsets.symmetric(vertical: 10.0, horizontal: 6.0),
                  ),
                  new Expanded(
                    child: TextField(
                      onChanged: (text){
                        setState(() {
                          noOfRoomsToBeBooked=text;
                        });
                        print("No of Rooms Booked $noOfRoomsToBeBooked");
                      },

                      cursorColor: ColorsMyApp.VividOrange,
                      inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                      keyboardType: TextInputType.number,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(color: Colors.black),
                        border: InputBorder.none,
                        hintText: StringsMyApp.enter_rooms_to_be_booked

                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: Button(
                    buttonText: StringsMyApp.next,
                    onButtonClickFunction: _clickButton,
                    buttonTextColor: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _clickButton(BuildContext context) {
    NavigationRouteUtil()
        .navigateToScreen(context, StringsMyApp.room_bill_detail_title);
  }
}

class InputBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: ColorsMyApp.editBoxBorderColor,
          width: 1,
        ),
        // borderRadius: BorderRadius.circular(20.0),
      ),
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 6.0),
          ),
          new Expanded(
            child: TextField(
              obscureText: false,
              cursorColor: ColorsMyApp.VividOrange,
              style: TextStyle(color: Colors.black),
              decoration: InputDecoration(
                labelStyle: TextStyle(color: Colors.white),
                border: InputBorder.none,
              ),
            ),
          )
        ],
      ),
    );
  }
}
