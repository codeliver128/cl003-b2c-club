import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/DateTimeFormatConstants.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils/NavigationRouteUtil.dart';
import 'package:delhi_gymkhana_club/utils_widget/BulletWidget.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:toast/toast.dart';

class RoomReservationsScreen extends StatefulWidget {
  @override
  _RoomReservationsScreenState createState() => _RoomReservationsScreenState();
}

class _RoomReservationsScreenState extends State<RoomReservationsScreen> {
  DateTime selectedCheckInDate, selectedCheckOutDate;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.room_reservation,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        margin: EdgeInsets.only(top: 16, left: 16, bottom: 16, right: 16),
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              StringsMyApp.checkin_date,
              style: TextStyle(
                  color: ColorsMyApp.themeColor,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Container(
              width: double.infinity,
              height: 50,
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.grey_border,
                  width: 1.5,
                ),
              ),
              child: InkWell(
                onTap: () {
                  _selectDateCheckIn();
                },
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 4,
                        child: Text(
                          selectedCheckInDate != null
                              ? new DateFormat(
                                      DateTimeFormatConstants.yyyy_MM_dd)
                                  .format(selectedCheckInDate)
                              : StringsMyApp.select_date,
                          style: TextStyle(
                              fontSize: 16,
                              color: selectedCheckInDate != null
                                  ? Colors.black
                                  : ColorsMyApp.grey_54),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Icon(
                            Icons.calendar_today,
                            color: ColorsMyApp.greyDark,
                            size: 22,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 26),
            ),
            Text(
              StringsMyApp.checkout_date,
              style: TextStyle(
                  color: ColorsMyApp.themeColor,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Container(
              width: double.infinity,
              height: 50,
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.grey_border,
                  width: 1.5,
                ),
              ),
              child: InkWell(
                onTap: () {
                  _selectDateCheckOut();
                },
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 4,
                        child: Text(
                          selectedCheckOutDate != null
                              ? new DateFormat(
                                      DateTimeFormatConstants.yyyy_MM_dd)
                                  .format(selectedCheckOutDate)
                              : StringsMyApp.select_date,
                          style: TextStyle(
                              fontSize: 16,
                              color: selectedCheckOutDate != null
                                  ? Colors.black
                                  : ColorsMyApp.grey_54),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Icon(
                            Icons.calendar_today,
                            color: ColorsMyApp.greyDark,
                            size: 22,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 32),
            ),
            Button(
              buttonText: StringsMyApp.submit,
              buttonBackGroundColor: ColorsMyApp.themeColor,
              onButtonClickFunction: _clickButton,
              buttonTextColor: Colors.white,
            ),
            Padding(
              padding: EdgeInsets.only(top: 36),
            ),
            NoteSection(),
          ],
        ),
      ),
    );
  }

  _selectDateCheckIn() async {
    var date = DateTime.now();
    selectedCheckInDate = await showDatePicker(
      context: context,
      initialDate: date,
      firstDate: new DateTime(date.year, date.month, date.day),
      lastDate: new DateTime(2020),
    );
    setState(() {
      if (selectedCheckOutDate == null) {
        selectedCheckOutDate = selectedCheckInDate.add(Duration(days: 1));
      }
    });
  }

  _selectDateCheckOut() async {
    if (selectedCheckInDate == null) {
      Toast.show(StringsMyApp.select_check_in_date, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      return;
    }

    selectedCheckOutDate = await showDatePicker(
      context: context,
      initialDate: selectedCheckInDate.add(Duration(days: 1)),
      firstDate: selectedCheckInDate.add(Duration(days: 1)),
      lastDate: selectedCheckInDate.add(Duration(days: 15)),
    );
    setState(() {
      var diff = selectedCheckOutDate.day - selectedCheckInDate.day;
      Toast.show(diff.toString(), context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    });
  }

  _clickButton(BuildContext context) {
    if (selectedCheckInDate == null) {
      Toast.show(StringsMyApp.select_check_in_date, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      return;
    }
    NavigationRouteUtil().navigateToScreen(context, StringsMyApp.room_booking);
  }
}

class NoteSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            StringsMyApp.note_colon,
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          Divider(),
          Text(
            StringsMyApp.online_room_booking,
            style: TextStyle(color: Colors.black87),
          ),
          BulletWidget(
            text: StringsMyApp.roombooking_note1,
            bulletColor: ColorsMyApp.greyDark,
          ),
          BulletWidget(
            text: StringsMyApp.roombooking_note2,
            bulletColor: ColorsMyApp.greyDark,
          ),
          BulletWidget(
            text: StringsMyApp.roombooking_note3,
            bulletColor: ColorsMyApp.greyDark,
          ),
        ],
      ),
    );
  }
}
