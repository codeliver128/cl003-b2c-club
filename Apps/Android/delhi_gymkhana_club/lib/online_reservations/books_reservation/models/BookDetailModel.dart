class BookDetailModel {
  String _bookTitle;
  String _bookAuthor;
  String _bookDate;
  String _bookDescription;
  String _bookAvailability;
  String _bookGenre;

  BookDetailModel(
      {bookTitle,
      bookAuthor,
      bookDate,
      bookDescription,
      bookAvailability,
      bookGenre})
      : _bookTitle = bookTitle,
        _bookAuthor = bookAuthor,
        _bookDate = bookDate,
        _bookDescription = bookDescription,
        _bookAvailability = bookAvailability,
        _bookGenre = bookGenre;

  String get bookTitle => _bookTitle;

  set bookTitle(String value) {
    _bookTitle = value;
  }

  String get bookAuthor => _bookAuthor;

  String get bookGenre => _bookGenre;

  set bookGenre(String value) {
    _bookGenre = value;
  }

  String get bookAvailability => _bookAvailability;

  set bookAvailability(String value) {
    _bookAvailability = value;
  }

  String get bookDescription => _bookDescription;

  set bookDescription(String value) {
    _bookDescription = value;
  }

  String get bookDate => _bookDate;

  set bookDate(String value) {
    _bookDate = value;
  }

  set bookAuthor(String value) {
    _bookAuthor = value;
  }

  getBooksList() {
    return [
      BookDetailModel(
          bookTitle: "The Hogwarts Library",
          bookAuthor: "J.K. Rowling",
          bookDate: "3 May 2017",
          bookAvailability: "Available",
          bookDescription:
              "A gorgeous new edition of the Hogwarts Library containing three much loved classics – Fantastic Beasts",
          bookGenre: "Fiction"),
      BookDetailModel(
          bookTitle: "Harry Potter 7 Volume",
          bookAuthor: "J.K. Rowling",
          bookDate: "3 May 2017",
          bookAvailability: "Unavailable",
          bookDescription:
              "A gorgeous new edition of the Hogwarts Library containing three much loved classics – Fantastic Beasts",
          bookGenre: "Fiction"),
      BookDetailModel(
          bookTitle: "Head First Java, 2nd Edition",
          bookAuthor: "Kathy Sierra",
          bookDate: "3 May 2017",
          bookAvailability: "Available",
          bookDescription:
              "Computer programming language Java is not easy to understand. It takes lot of time and practice to understand the complex programming language. But this book takes an interactive and fun approach for better understanding of different fundamentals of Java. The book offers multi-sensory and fun learning experience for new programmers, so they can easily pick up the new language.Human brain responds and remembers unusual situations, images or incidents. Keeping that in mind, this revised second edition of the book has been presented with lots of humorous images, memorable analogies and mind-bending exercises. The language in which the book is written is also casual for better understanding of the difficult computer programming language.Although it follows a causal approach, the book refers to some serious computer-related information that encourages readers to learn and think like professional Java programmers. The book focuses on Java 5.0, the latest version of the Java development platform, along with certain deep code-levels. From basic programming fundamentals to advanced topics, including threads, distributed programming with RMI and network sockets, the book covers almost all major Java concepts.To make the learning and remembering tasks easier, the book comes with numerous puzzles, striking visuals, mysterious problems and certain soul-searching interviews to make the computer programming more engaging and playful. For new programmers and those who want to brush up their programming knowledge, this book may be a good companion. With this easy-to-understand book, learning complex computer language shall no more be a difficult task.",
          bookGenre: "Fiction"),
      BookDetailModel(
          bookTitle: "The Hogwarts Library",
          bookAuthor: "J.K. Rowling",
          bookDate: "3 May 2017",
          bookAvailability: "Available",
          bookDescription:
              "A gorgeous new edition of the Hogwarts Library containing three much loved classics – Fantastic Beasts",
          bookGenre: "Fiction"),
      BookDetailModel(
          bookTitle: "The Hogwarts Library",
          bookAuthor: "J.K. Rowling",
          bookDate: "3 May 2017",
          bookAvailability: "Unavailable",
          bookDescription:
              "A gorgeous new edition of the Hogwarts Library containing three much loved classics – Fantastic Beasts",
          bookGenre: "Fiction"),
      BookDetailModel(
          bookTitle: "The Hogwarts Library",
          bookAuthor: "J.K. Rowling",
          bookDate: "3 May 2017",
          bookAvailability: "Available",
          bookDescription:
              "A gorgeous new edition of the Hogwarts Library containing three much loved classics – Fantastic Beasts",
          bookGenre: "Fiction"),
      BookDetailModel(
          bookTitle: "The Hogwarts Library",
          bookAuthor: "J.K. Rowling",
          bookDate: "3 May 2017",
          bookAvailability: "Available",
          bookDescription:
              "A gorgeous new edition of the Hogwarts Library containing three much loved classics – Fantastic Beasts",
          bookGenre: "Fiction"),
      BookDetailModel(
          bookTitle: "The Hogwarts Library",
          bookAuthor: "J.K. Rowling",
          bookDate: "3 May 2017",
          bookAvailability: "Unavailable",
          bookDescription:
              "A gorgeous new edition of the Hogwarts Library containing three much loved classics – Fantastic Beasts",
          bookGenre: "Fiction"),
      BookDetailModel(
          bookTitle: "The Hogwarts Library",
          bookAuthor: "J.K. Rowling",
          bookDate: "3 May 2017",
          bookAvailability: "Available",
          bookDescription:
              "A gorgeous new edition of the Hogwarts Library containing three much loved classics – Fantastic Beasts",
          bookGenre: "Fiction"),
      BookDetailModel(
          bookTitle: "The Hogwarts Library",
          bookAuthor: "J.K. Rowling",
          bookDate: "3 May 2017",
          bookAvailability: "Available",
          bookDescription:
              "A gorgeous new edition of the Hogwarts Library containing three much loved classics – Fantastic Beasts",
          bookGenre: "Fiction"),
      BookDetailModel(
          bookTitle: "The Hogwarts Library",
          bookAuthor: "J.K. Rowling",
          bookDate: "3 May 2017",
          bookAvailability: "Available",
          bookDescription:
              "A gorgeous new edition of the Hogwarts Library containing three much loved classics – Fantastic Beasts",
          bookGenre: "Fiction"),
    ];
  }
}
