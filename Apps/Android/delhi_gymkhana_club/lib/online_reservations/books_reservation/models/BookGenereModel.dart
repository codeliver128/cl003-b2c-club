class BookGenereModel {
  String _id;
  String _bookGenre;

  BookGenereModel({id, bookGenre})
      : _id = id,
        _bookGenre = bookGenre;

  String get id => _id;

  set id(String value) {
    _id = value;
  }


  String get bookGenre => _bookGenre;

  set bookGenre(String value) {
    _bookGenre = value;
  }

  getBookTypeList() {
    return [
      BookGenereModel(id: "1", bookGenre: "Action and adventure"),
      BookGenereModel(id: "2", bookGenre: "Autobiography"),
      BookGenereModel(id: "3", bookGenre: "Crime"),
      BookGenereModel(id: "4", bookGenre: "Health"),
      BookGenereModel(id: "5", bookGenre: "Fiction"),
      BookGenereModel(id: "6", bookGenre: "Non-fiction"),
      BookGenereModel(id: "7", bookGenre: "Health"),
      BookGenereModel(id: "8", bookGenre: "History"),
      BookGenereModel(id: "9", bookGenre: "Science"),
      BookGenereModel(id: "10", bookGenre: "Suspense"),
    ];
  }
}
