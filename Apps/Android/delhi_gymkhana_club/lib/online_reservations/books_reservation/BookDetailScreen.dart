import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/Constants.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/online_reservations/books_reservation/models/BookDetailModel.dart';
import 'package:delhi_gymkhana_club/utils_widget/Button.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';

class BookDetailScreen extends StatefulWidget {
  @override
  _BookDetailScreenState createState() => _BookDetailScreenState();
}

class _BookDetailScreenState extends State<BookDetailScreen> {
  BookDetailModel bookDetailModel;

  @override
  Widget build(BuildContext context) {
    bookDetailModel = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.book_detail,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        margin: EdgeInsets.only(top: 26, left: 16, bottom: 16, right: 16),
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Align(
              alignment: Alignment.centerRight,
              child: Text(
                bookDetailModel.bookAvailability,
                style: TextStyle(
                    color: bookDetailModel.bookAvailability ==
                            StringsMyApp.available
                        ? ColorsMyApp.green
                        : ColorsMyApp.red),
              ),
            ),
            Text(
              bookDetailModel.bookTitle,
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
            Padding(
              padding: EdgeInsets.only(top: 6),
            ),
            Text(
              "by " +
                  bookDetailModel.bookAuthor +
                  " | " +
                  bookDetailModel.bookDate,
              style: TextStyle(fontSize: 14, color: Colors.black),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Text(
              bookDetailModel.bookDescription,
              style: TextStyle(fontSize: 12, color: ColorsMyApp.greyDark),
            ),
            Padding(
              padding: EdgeInsets.only(top: 16),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: Button(
                    buttonText: bookDetailModel.bookAvailability ==
                            StringsMyApp.available
                        ? StringsMyApp.reserve
                        : StringsMyApp.request,
                    buttonBackGroundColor: bookDetailModel.bookAvailability ==
                            StringsMyApp.available
                        ? Colors.black
                        : ColorsMyApp.greyDark,
                    onButtonClickFunction: _clickButton,
                    buttonTextColor: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _clickButton(BuildContext context) {
    Toast.show(bookDetailModel.bookAvailability ==
        StringsMyApp.available
        ?"Book Reserved Successfully.":"Book Reservation Request Submitted Successfully.", context,
        gravity: Toast.BOTTOM, duration: Toast.LENGTH_LONG);

    Navigator.popUntil(context, ModalRoute.withName(HOME_SCREEN));

  }
}
