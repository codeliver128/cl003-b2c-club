import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/online_reservations/books_reservation/models/BookDetailModel.dart';
import 'package:delhi_gymkhana_club/online_reservations/books_reservation/models/BookGenereModel.dart';
import 'package:delhi_gymkhana_club/utils/NavigationRouteUtil.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:delhi_gymkhana_club/constants/Constants.dart';
import 'package:toast/toast.dart';

class BooksReservationScreen extends StatefulWidget {
  @override
  _BooksReservationScreenState createState() => _BooksReservationScreenState();
}

class _BooksReservationScreenState extends State<BooksReservationScreen> {
  List<BookDetailModel> listBooks = BookDetailModel().getBooksList();

  BookGenereModel _selectedGenre;
  List<BookGenereModel> listBookGenres = BookGenereModel().getBookTypeList();
  List<DropdownMenuItem<BookGenereModel>> _dropdownMenuItems;

  @override
  void initState() {
    _dropdownMenuItems = buildDropdownMenuItems(listBookGenres);
    super.initState();
  }

  List<DropdownMenuItem<BookGenereModel>> buildDropdownMenuItems(List generes) {
    List<DropdownMenuItem<BookGenereModel>> items = List();
    for (BookGenereModel genre in generes) {
      items.add(
        DropdownMenuItem(
          value: genre,
          child: Text(genre.bookGenre),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(BookGenereModel selectedGenre) {
    setState(() {
      _selectedGenre = selectedGenre;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.books_reservation,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 4, bottom: 16),
        child: Column(
          children: <Widget>[
            Container(
              padding:
                  EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
              color: ColorsMyApp.themeColor,
              child: Container(
                padding: EdgeInsets.only(right: 10),
                color: Colors.white,
                child: Row(
                  children: <Widget>[
                    new Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 10.0, horizontal: 6.0),
                    ),
                    new Expanded(
                      flex: 6,
                      child: TextField(
                        obscureText: false,
                        cursorColor: ColorsMyApp.VividOrange,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                          labelStyle: TextStyle(color: Colors.black),
                          border: InputBorder.none,
                          hintText: StringsMyApp.author_title,
                          hintStyle: TextStyle(color: ColorsMyApp.grey_border),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Icon(
                          Icons.search,
                          color: ColorsMyApp.greyDark,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 15, right: 15, top: 12, bottom: 12),
              padding: EdgeInsets.only(left: 10, right: 10),
              width: double.infinity,
              height: 48,
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.grey_border,
                  width: 1.5,
                ),
              ),
              child: DropdownButton(
                  underline: Container(),
                  isExpanded: true,
                  icon: Icon(
                    Icons.keyboard_arrow_down,
                    color: ColorsMyApp.greyDark,
                    size: 32,
                  ),
                  hint: Text(
                    StringsMyApp.category,
                    style: TextStyle(color: ColorsMyApp.grey_54, fontSize: 16),
                  ),
                  value: _selectedGenre,
                  items: _dropdownMenuItems,
                  onChanged: onChangeDropdownItem),
            ),
            Expanded(
                child: Container(
              child: ListView.builder(
                itemCount: listBooks.length,
                itemBuilder: (context, position) {
                  return _getListRow(context, position);
                },
              ),
            )),
          ],
        ),
      ),
    );
  }

  _getListRow(BuildContext context, int position) {
    return Card(
      elevation: 4,
      color: ColorsMyApp.grey_border,
      child: InkWell(
        onTap: () {
          NavigationRouteUtil().navigateToScreen(
              context, StringsMyApp.book_detail,
              arguments: listBooks[position]);
        },
        child: Container(
          padding: EdgeInsets.only(left: 10, right: 10, top: 14, bottom: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                listBooks[position].bookTitle,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              ),
              Padding(
                padding: EdgeInsets.only(top: 4),
              ),
              Text(
                "by " +
                    listBooks[position].bookAuthor +
                    " | " +
                    listBooks[position].bookDate,
                style: TextStyle(fontSize: 14, color: ColorsMyApp.greyDark),
              ),
              Padding(
                padding: EdgeInsets.only(top: 4),
              ),
              Text(
                listBooks[position].bookDescription,
                maxLines: 3,
                style: TextStyle(fontSize: 12, color: ColorsMyApp.greyDark),
              ),
              Padding(
                padding: EdgeInsets.only(top: 16),
              ),
              Row(
                children: <Widget>[
                  Text(
                    listBooks[position].bookAvailability,
                    style: TextStyle(
                        color: listBooks[position].bookAvailability ==
                                StringsMyApp.available
                            ? ColorsMyApp.green
                            : ColorsMyApp.red),
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: RaisedButton(
                        onPressed: () {
                          Toast.show(listBooks[position].bookAvailability ==
                              StringsMyApp.available
                              ?"Book Reserved Successfully.":"Book Reservation Request Submitted Successfully.", context,
                              gravity: Toast.BOTTOM, duration: Toast.LENGTH_LONG);

                          Navigator.popUntil(context, ModalRoute.withName(HOME_SCREEN));

                        },
                        child: Text(
                          listBooks[position].bookAvailability ==
                                  StringsMyApp.available
                              ? StringsMyApp.reserve
                              : StringsMyApp.request,
                          style: TextStyle(color: Colors.white),
                        ),
                        elevation: 3,
                        color: listBooks[position].bookAvailability ==
                                StringsMyApp.available
                            ? Colors.black
                            : ColorsMyApp.greyDark,
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
