class WebviewArguments {
  final String url;
  final String title;

  WebviewArguments({this.url, this.title});
}
