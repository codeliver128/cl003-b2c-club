import 'dart:core';

class EventsDataModel {
  var _imgUrl;
  var _eventTitle;
  var _eventDate;
  var _eventDescription;

  EventsDataModel({imgUrl, eventTitle, eventDate, eventDescription})
      : _imgUrl = imgUrl,
        _eventDate = eventDate,
        _eventTitle = eventTitle,
        _eventDescription = eventDescription;

  get imgUrl => _imgUrl;

  set imgUrl(value) {
    _imgUrl = value;
  }

  get eventDescription => _eventDescription;

  set eventDescription(value) {
    _eventDescription = value;
  }

  get eventDate => _eventDate;

  set eventDate(value) {
    _eventDate = value;
  }

  get eventTitle => _eventTitle;

  set eventTitle(value) {
    _eventTitle = value;
  }
}
