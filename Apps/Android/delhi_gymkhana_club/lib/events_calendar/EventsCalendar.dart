import 'package:delhi_gymkhana_club/constants/ColorsMyApp.dart';
import 'package:delhi_gymkhana_club/constants/StringsMyApp.dart';
import 'package:delhi_gymkhana_club/utils_widget/HomeIconToolbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'EventsDataModel.dart';

class EventsCalendar extends StatefulWidget {
  @override
  _EventsCalendarState createState() => _EventsCalendarState();
}

class _EventsCalendarState extends State<EventsCalendar> {
  List<EventsDataModel> listEventsModel;

  String _selectedMonth;
  List<String> listMonths = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  List<DropdownMenuItem<String>> _dropdownMenuItems;

  @override
  void initState() {
    _dropdownMenuItems = buildDropdownMenuItems(listMonths);
    super.initState();
  }

  List<DropdownMenuItem<String>> buildDropdownMenuItems(List months) {
    List<DropdownMenuItem<String>> items = List();
    for (String value in months) {
      items.add(
        DropdownMenuItem(
          value: value,
          child: Text(value),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(String selectedMonth) {
    setState(() {
      _selectedMonth = selectedMonth;
    });
  }

  @override
  Widget build(BuildContext context) {
    listEventsModel = new List();
    listEventsModel.add(new EventsDataModel(
        imgUrl:
            "https://cdn.pixabay.com/photo/2016/01/08/11/57/butterfly-1127666__340.jpg",
        eventDate: "12 Aug 2019",
        eventTitle: "Kargil Diwas",
        eventDescription:
            "Kargil Vijay Diwas, named after the successful Operation Vijay, is celebrated in India on 26 July. On this date in 1999, India successfully took command of the high outposts which had been lost to Pakistan. The Kargil war was fought for more than 60 days, ended on 26 July and resulted in loss of life on both the sides. The war ended with India regaining control of all the previously held territory, hence re-establishing the status quo ante bellum. Kargil Vijay Diwas is celebrated on 26 July every year in honour."));
    listEventsModel.add(new EventsDataModel(
        imgUrl:
            "https://cdn.pixabay.com/photo/2016/02/17/19/08/lotus-1205631__340.jpg",
        eventDate: "15 Aug 2019",
        eventTitle: "Independence Day",
        eventDescription:
            "Kargil Vijay Diwas, named after the successful Operation Vijay, is celebrated in India on 26 July. On this date in 1999, India successfully took command of the high outposts which had been lost to Pakistan. The Kargil war was fought for more than 60 days, ended on 26 July and resulted in loss of life on both the sides. The war ended with India regaining control of all the previously held territory, hence re-establishing the status quo ante bellum. Kargil Vijay Diwas is celebrated on 26 July every year in honour of the Kargil War's Heroes. This day is celebrated in the Kargil–Dras sector and the national capital New Delhi, where the Prime Minister of India pays homage to the soldiers at Amar Jawan Jyoti at India Gate every year. Functions are also organized all over the country to commemorate the contributions of the armed forces."));
    listEventsModel.add(new EventsDataModel(
        imgUrl:
            "https://cdn.pixabay.com/photo/2014/09/14/18/04/dandelion-445228__340.jpg",
        eventDate: "12 Aug 2019",
        eventTitle: "Yoga Day",
        eventDescription:
            "Kargil Vijay Diwas, named after the successful Operation Vijay, is celebrated in India on 26 July. On this date in 1999, India successfully took command of the high outposts which had been lost to Pakistan. The Kargil war was fought for more than 60 days, ended on 26 July and resulted in loss of life on both the sides. The war ended with India regaining control of all the previously held territory, hence re-establishing the status quo ante bellum. Kargil Vijay Diwas is celebrated on 26 July every year in honour of the Kargil War's Heroes. This day is celebrated in the Kargil–Dras sector and the national capital New Delhi, where the Prime Minister of India pays homage to the soldiers at Amar Jawan Jyoti at India Gate every year. Functions are also organized all over the country to commemorate the contributions of the armed forces."));
    listEventsModel.add(new EventsDataModel(
        imgUrl:
            "https://cdn.pixabay.com/photo/2015/04/19/08/32/rose-729509__340.jpg",
        eventDate: "12 Aug 2019",
        eventTitle: "Golf Lessons",
        eventDescription:
            "Kargil Vijay Diwas, named after the successful Operation Vijay, is celebrated in India on 26 July. On this date in 1999, India successfully took command of the high outposts which had been lost to Pakistan. The Kargil war was fought for more than 60 days, ended on 26 July and resulted in loss of life on both the sides. The war ended with India regaining control of all the previously held territory, hence re-establishing the status quo ante bellum. Kargil Vijay Diwas is celebrated on 26 July every year in honour of the Kargil War's Heroes. This day is celebrated in the Kargil–Dras sector and the national capital New Delhi, where the Prime Minister of India pays homage to the soldiers at Amar Jawan Jyoti at India Gate every year. Functions are also organized all over the country to commemorate the contributions of the armed forces."));
    listEventsModel.add(new EventsDataModel(
        imgUrl:
            "https://cdn.pixabay.com/photo/2014/12/17/21/35/drop-of-water-571956__340.jpg",
        eventDate: "12 Aug 2019",
        eventTitle: "Vatika",
        eventDescription:
            "Kargil Vijay Diwas, named after the successful Operation Vijay, is celebrated in India on 26 July. On this date in 1999, India successfully took command of the high outposts which had been lost to Pakistan. The Kargil war was fought for more than 60 days, ended on 26 July and resulted in loss of life on both the sides. The war ended with India regaining control of all the previously held territory, hence re-establishing the status quo ante bellum. Kargil Vijay Diwas is celebrated on 26 July every year in honour of the Kargil War's Heroes. This day is celebrated in the Kargil–Dras sector and the national capital New Delhi, where the Prime Minister of India pays homage to the soldiers at Amar Jawan Jyoti at India Gate every year. Functions are also organized all over the country to commemorate the contributions of the armed forces."));
    listEventsModel.add(new EventsDataModel(
        imgUrl:
            "https://cdn.pixabay.com/photo/2016/10/25/22/22/roses-1770165__340.png",
        eventDate: "12 Aug 2019",
        eventTitle: "Diwali",
        eventDescription:
            "Kargil Vijay Diwas, named after the successful Operation Vijay, is celebrated in India on 26 July. On this date in 1999, India successfully took command of the high outposts which had been lost to Pakistan. The Kargil war was fought for more than 60 days, ended on 26 July and resulted in loss of life on both the sides. The war ended with India regaining control of all the previously held territory, hence re-establishing the status quo ante bellum. Kargil Vijay Diwas is celebrated on 26 July every year in honour of the Kargil War's Heroes. This day is celebrated in the Kargil–Dras sector and the national capital New Delhi, where the Prime Minister of India pays homage to the soldiers at Amar Jawan Jyoti at India Gate every year. Functions are also organized all over the country to commemorate the contributions of the armed forces."));
    listEventsModel.add(new EventsDataModel(
        imgUrl:
            "https://cdn.pixabay.com/photo/2014/05/03/00/50/flower-child-336658__340.jpg",
        eventDate: "12 Aug 2019",
        eventTitle: "Holi",
        eventDescription:
            "Kargil Vijay Diwas, named after the successful Operation Vijay, is celebrated in India on 26 July. On this date in 1999, India successfully took command of the high outposts which had been lost to Pakistan. The Kargil war was fought for more than 60 days, ended on 26 July and resulted in loss of life on both the sides. The war ended with India regaining control of all the previously held territory, hence re-establishing the status quo ante bellum. Kargil Vijay Diwas is celebrated on 26 July every year in honour of the Kargil War's Heroes. This day is celebrated in the Kargil–Dras sector and the national capital New Delhi, where the Prime Minister of India pays homage to the soldiers at Amar Jawan Jyoti at India Gate every year. Functions are also organized all over the country to commemorate the contributions of the armed forces."));
    listEventsModel.add(new EventsDataModel(
        imgUrl:
            "https://cdn.pixabay.com/photo/2015/07/09/22/44/tree-838666__340.jpg",
        eventDate: "12 Aug 2019",
        eventTitle: "Navratra",
        eventDescription:
            "Kargil Vijay Diwas, named after the successful Operation Vijay, is celebrated in India on 26 July. On this date in 1999, India successfully took command of the high outposts which had been lost to Pakistan. The Kargil war was fought for more than 60 days, ended on 26 July and resulted in loss of life on both the sides. The war ended with India regaining control of all the previously held territory, hence re-establishing the status quo ante bellum. Kargil Vijay Diwas is celebrated on 26 July every year in honour of the Kargil War's Heroes. This day is celebrated in the Kargil–Dras sector and the national capital New Delhi, where the Prime Minister of India pays homage to the soldiers at Amar Jawan Jyoti at India Gate every year. Functions are also organized all over the country to commemorate the contributions of the armed forces."));
    listEventsModel.add(new EventsDataModel(
        imgUrl:
            "https://cdn.pixabay.com/photo/2019/03/21/23/04/tulips-4072214__340.jpg",
        eventDate: "12 Aug 2019",
        eventTitle: "Dussherra",
        eventDescription:
            "Kargil Vijay Diwas, named after the successful Operation Vijay, is celebrated in India on 26 July. On this date in 1999, India successfully took command of the high outposts which had been lost to Pakistan. The Kargil war was fought for more than 60 days, ended on 26 July and resulted in loss of life on both the sides. The war ended with India regaining control of all the previously held territory, hence re-establishing the status quo ante bellum. Kargil Vijay Diwas is celebrated on 26 July every year in honour of the Kargil War's Heroes. This day is celebrated in the Kargil–Dras sector and the national capital New Delhi, where the Prime Minister of India pays homage to the soldiers at Amar Jawan Jyoti at India Gate every year. Functions are also organized all over the country to commemorate the contributions of the armed forces."));
    listEventsModel.add(new EventsDataModel(
        imgUrl:
            "https://cdn.pixabay.com/photo/2016/07/11/15/43/pretty-woman-1509956__340.jpg",
        eventDate: "12 Aug 2019",
        eventTitle: "Christmas",
        eventDescription:
            "Kargil Vijay Diwas, named after the successful Operation Vijay, is celebrated in India on 26 July. On this date in 1999, India successfully took command of the high outposts which had been lost to Pakistan. The Kargil war was fought for more than 60 days, ended on 26 July and resulted in loss of life on both the sides. The war ended with India regaining control of all the previously held territory, hence re-establishing the status quo ante bellum. Kargil Vijay Diwas is celebrated on 26 July every year in honour of the Kargil War's Heroes. This day is celebrated in the Kargil–Dras sector and the national capital New Delhi, where the Prime Minister of India pays homage to the soldiers at Amar Jawan Jyoti at India Gate every year. Functions are also organized all over the country to commemorate the contributions of the armed forces."));
    listEventsModel.add(new EventsDataModel(
        imgUrl:
            "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510__340.jpg",
        eventDate: "12 Aug 2019",
        eventTitle: "Teez",
        eventDescription:
            "Kargil Vijay Diwas, named after the successful Operation Vijay, is celebrated in India on 26 July. On this date in 1999, India successfully took command of the high outposts which had been lost to Pakistan. The Kargil war was fought for more than 60 days, ended on 26 July and resulted in loss of life on both the sides. The war ended with India regaining control of all the previously held territory, hence re-establishing the status quo ante bellum. Kargil Vijay Diwas is celebrated on 26 July every year in honour of the Kargil War's Heroes. This day is celebrated in the Kargil–Dras sector and the national capital New Delhi, where the Prime Minister of India pays homage to the soldiers at Amar Jawan Jyoti at India Gate every year. Functions are also organized all over the country to commemorate the contributions of the armed forces."));
    listEventsModel.add(new EventsDataModel(
        imgUrl:
            "https://cdn.pixabay.com/photo/2018/04/21/20/23/tulips-3339416__340.jpg",
        eventDate: "12 Aug 2019",
        eventTitle: "Mela",
        eventDescription:
            "Kargil Vijay Diwas, named after the successful Operation Vijay, is celebrated in India on 26 July. On this date in 1999, India successfully took command of the high outposts which had been lost to Pakistan. The Kargil war was fought for more than 60 days, ended on 26 July and resulted in loss of life on both the sides. The war ended with India regaining control of all the previously held territory, hence re-establishing the status quo ante bellum. Kargil Vijay Diwas is celebrated on 26 July every year in honour of the Kargil War's Heroes. This day is celebrated in the Kargil–Dras sector and the national capital New Delhi, where the Prime Minister of India pays homage to the soldiers at Amar Jawan Jyoti at India Gate every year. Functions are also organized all over the country to commemorate the contributions of the armed forces."));
    listEventsModel.add(new EventsDataModel(
        imgUrl:
            "https://cdn.pixabay.com/photo/2019/07/15/06/16/cactus-4338616__340.png",
        eventDate: "31 Dec 2019",
        eventTitle: "New Year",
        eventDescription:
            "Kargil Vijay Diwas, named after the successful Operation Vijay, is celebrated in India on 26 July. On this date in 1999, India successfully took command of the high outposts which had been lost to Pakistan. The Kargil war was fought for more than 60 days, ended on 26 July and resulted in loss of life on both the sides. The war ended with India regaining control of all the previously held territory, hence re-establishing the status quo ante bellum. Kargil Vijay Diwas is celebrated on 26 July every year in honour of the Kargil War's Heroes. This day is celebrated in the Kargil–Dras sector and the national capital New Delhi, where the Prime Minister of India pays homage to the soldiers at Amar Jawan Jyoti at India Gate every year. Functions are also organized all over the country to commemorate the contributions of the armed forces."));
    listEventsModel.add(new EventsDataModel(
        imgUrl:
            "https://cdn.pixabay.com/photo/2019/07/07/15/01/tulip-4322635__340.jpg",
        eventDate: "12 Aug 2019",
        eventTitle: "Dhanteras",
        eventDescription:
            "Kargil Vijay Diwas, named after the successful Operation Vijay, is celebrated in India on 26 July. On this date in 1999, India successfully took command of the high outposts which had been lost to Pakistan. The Kargil war was fought for more than 60 days, ended on 26 July and resulted in loss of life on both the sides. The war ended with India regaining control of all the previously held territory, hence re-establishing the status quo ante bellum. Kargil Vijay Diwas is celebrated on 26 July every year in honour of the Kargil War's Heroes. This day is celebrated in the Kargil–Dras sector and the national capital New Delhi, where the Prime Minister of India pays homage to the soldiers at Amar Jawan Jyoti at India Gate every year. Functions are also organized all over the country to commemorate the contributions of the armed forces."));

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[HomeIconToolbar()],
        iconTheme: new IconThemeData(color: ColorsMyApp.hamburgerOrBackIcon),
        title: new Text(
          StringsMyApp.events_calendar,
          style: TextStyle(color: ColorsMyApp.VividOrange),
        ),
        backgroundColor: Colors.white,
        elevation: 4,
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 10, left: 12, right: 10),
              child: Text(
                StringsMyApp.filter_by,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 2, left: 10, right: 10, bottom: 4),
              padding: EdgeInsets.only(left: 10, right: 10),
              width: double.infinity,
              height: 48,
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorsMyApp.grey_border,
                  width: 1.5,
                ),
              ),
              child: DropdownButton(
                  underline: Container(),
                  isExpanded: true,
                  icon: Icon(
                    Icons.keyboard_arrow_down,
                    color: ColorsMyApp.greyDark,
                    size: 32,
                  ),
                  hint: Text(
                    StringsMyApp.select_month,
                    style: TextStyle(color: ColorsMyApp.grey_54, fontSize: 16),
                  ),
                  value: _selectedMonth,
                  items: _dropdownMenuItems,
                  onChanged: onChangeDropdownItem),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: listEventsModel.length,
                itemBuilder: (context, position) {
                  return _getListRow(context, position);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  _getListRow(BuildContext context, int position) {
    return Card(
      elevation: 5,
      margin: EdgeInsets.only(left: 10, right: 10, top: 10),
      color: ColorsMyApp.navigationBackGround,
      child: InkWell(
        onTap: () {
          _askUser(position);
        },
        child: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(7),
              child: ClipRRect(
                borderRadius: new BorderRadius.circular(10.0),
                child: Image.network(
                  listEventsModel[position].imgUrl,
                  width: 120,
                  height: 140,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 8),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(right: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      listEventsModel[position].eventTitle,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 5),
                    ),
                    Text(listEventsModel[position].eventDate,
                        style: TextStyle(
                          color: ColorsMyApp.greyDark,
                          fontSize: 11,
                        )),
                    Padding(
                      padding: EdgeInsets.only(bottom: 5),
                    ),
                    Text(listEventsModel[position].eventDescription,
                        maxLines: 4,
                        style: TextStyle(
                          color: ColorsMyApp.greyDark,
                          fontSize: 12,
                        ))
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future _askUser(int position) async {
    await showDialog(
        context: context,
        /*it shows a popup with few options which you can select, for option we
        created enums which we can use with switch statement, in this first switch
        will wait for the user to select the option which it can use with switch cases*/
        child: Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0),
          ),
          elevation: 0.0,
          backgroundColor: Colors.white,
          child: dialogContent(context, position),
        ));
  }

  dialogContent(BuildContext context, int position) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              children: <Widget>[
                Image(
                    width: 120,
                    height: 140,
                    fit: BoxFit.cover,
                    image: new NetworkImage(
                      listEventsModel[position].imgUrl,
                    )),
                Padding(
                  padding: EdgeInsets.only(left: 20),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        listEventsModel[position].eventTitle,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                        ),
                      ),
                      Text(listEventsModel[position].eventDate,
                          style: TextStyle(
                            color: ColorsMyApp.greyDark,
                            fontSize: 12,
                          )),
                    ],
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            Expanded(
              flex: 6,
              child: SingleChildScrollView(
                child: Text(
                  listEventsModel[position].eventDescription,
                  style: TextStyle(color: ColorsMyApp.grey_54, fontSize: 15),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.bottomRight,
                child: FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(StringsMyApp.ok)),
              ),
            )
          ],
        ));
  }
}
